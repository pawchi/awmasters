package com.chilon.awmasters.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.chilon.awmasters.data.Master
import com.chilon.awmasters.data.MasterFav
import com.chilon.awmasters.repository.LocalMasterRepository
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.runBlocking

class MasterFavViewModel(application: Application):AndroidViewModel(application) {
    private val localRepo = LocalMasterRepository(application)


    fun insertMasterFav(masterFav: MasterFav){
        localRepo.insertMasterFav(masterFav)
    }

    fun updateMasterFav(masterFav: MasterFav){
        localRepo.updateMasterFav(masterFav)
    }

    fun deleteMasterFav(masterFav: MasterFav){
        localRepo.deleteMasterFav(masterFav)
    }

    fun deleteMasterFavWithNickName(nickName: String){
        localRepo.deleteMasterFavWithNickName(nickName)
    }

    fun deleteAllMasterFav(){
        localRepo.deleteAllMasterFav()
    }

    fun getAllMastersFav():LiveData<List<MasterFav>>{
        return runBlocking {
            localRepo.getAllMasterFav().await()
        }
    }

    fun getMasterFavWithNickName(nickName:String):LiveData<List<MasterFav>>{
        return runBlocking {
            localRepo.getMasterFavWithNickName(nickName).await()
        }
    }
}