package com.chilon.awmasters.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.chilon.awmasters.data.MovieLocal
import com.chilon.awmasters.repository.LocalMovieRepository
import kotlinx.coroutines.runBlocking

class MovieViewModel(application: Application):AndroidViewModel(application) {
    private val localRepo = LocalMovieRepository(application)


    fun getAllMovies():LiveData<List<MovieLocal>>{
        return runBlocking {
            localRepo.getAllMoviesLocal().await()
        }
    }

    fun updateMovie(movieLocal: MovieLocal){
        localRepo.updateMovieLocal(movieLocal)
    }

    fun getAllMoviesLocalOneTime(): List<MovieLocal>{
        return runBlocking {
            localRepo.getAllMoviesLocalOneTime().await()
        }
    }
}