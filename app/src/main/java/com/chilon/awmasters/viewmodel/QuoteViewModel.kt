package com.chilon.awmasters.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.chilon.awmasters.data.Quote
import com.chilon.awmasters.repository.LocalQuoteRepository
import kotlinx.coroutines.runBlocking

class QuoteViewModel(application: Application): AndroidViewModel(application) {
    private val localQuoteRepo = LocalQuoteRepository(application)

    fun insertQuote(quote: Quote){
        localQuoteRepo.insertQuote(quote)
    }

    fun getAllQuotes():LiveData<List<Quote>>{
        return runBlocking {
            localQuoteRepo.getAllQuotes().await()
        }
    }
}