package com.chilon.awmasters.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.chilon.awmasters.repository.FirebaseRepository
import com.google.firebase.auth.AuthCredential

class FirebaseViewModel(application: Application): AndroidViewModel(application) {
    private val repository = FirebaseRepository(application)
    val masterListLiveData = repository.fBRepoMasterListLiveData
    val registerLiveData = repository.fBRepoRegisterFbLiveData
    val removeAccountLiveData = repository.fBRepoDeleteFbUserLiveData
    val movieListLiveData = repository.fBRepoMovieListLiveData
    val quoteListLiveData = repository.fBRepoQuoteListLiveData

    init {
        repository.getAllMasters()
        repository.getAllMovies()
        repository.getAllQuotes()
    }

//    fun actionRemoveUserAccount(){
//        repository.removeUserAccount()
//    }

    fun actionUpdateLikesInFB(docId: String, likesNumber:Long){
        repository.updateMastersLikesInFB(docId,likesNumber)
    }

//    fun actionRegister(credential: AuthCredential){
//        repository.authWithCredential(credential)
//    }
//
//    fun actionLogin(credential: AuthCredential){
//        repository.loginWithCredential(credential)
//    }

    fun actionUpdateMovieLikesInFB(title:String, likesNumber:Int){
        repository.updateMovieLikes(title,likesNumber)
    }

}