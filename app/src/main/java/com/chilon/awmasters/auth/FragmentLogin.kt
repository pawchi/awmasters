package com.chilon.awmasters.auth

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.chilon.awmasters.R
import com.chilon.awmasters.utilis.AppConstants
import com.chilon.awmasters.utilis.AppUtils
import com.chilon.awmasters.viewmodel.FirebaseViewModel
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.GoogleAuthProvider

class FragmentLogin : Fragment() {
//    private lateinit var progressBar: ProgressBar
//    private lateinit var inputEmail: EditText
//    private lateinit var inputPass: EditText
//    private lateinit var loginButton: Button
//    private lateinit var firebaseViewModel: FirebaseViewModel
//    private lateinit var loginGoogle: LinearLayout
//    private lateinit var myView: View
//    private lateinit var googleSignInClient: GoogleSignInClient
//
//    override fun onCreateView(
//        inflater: LayoutInflater, container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        myView = inflater.inflate(R.layout.fragment_login, container, false)
//
//        firebaseViewModel = ViewModelProvider(requireActivity()).get(FirebaseViewModel::class.java)
//
//        progressBar = myView.findViewById(R.id.fr_login__progress_bar)
//        inputEmail = myView.findViewById(R.id.fr_login__email_edit_text)
//        inputPass = myView.findViewById(R.id.fr_login__pass_edit_text)
//        loginButton = myView.findViewById(R.id.fr_login__btn_login)
//        loginGoogle = myView.findViewById(R.id.fr_login__layout_login_google)
//
//        loginButton.setOnClickListener {
//            progressBar.visibility = View.VISIBLE
//            val credential = EmailAuthProvider.getCredential(inputEmail.text.toString(),inputPass.text.toString())
//            firebaseViewModel.actionLogin(credential)
//        }
//
//
//        val inputTextWatcher = object : TextWatcher {
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
//            override fun afterTextChanged(s: Editable?) {}
//
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//                val email = inputEmail.text.toString()
//                val pass = inputPass.text.toString()
//                val emailCheck: Boolean
//                val passCheck: Boolean
//                val fieldOk = ContextCompat.getColor(myView.context, R.color.text_green)
//                val fieldNotOk = ContextCompat.getColor(myView.context, R.color.color_black)
//
//                emailCheck = if (AppUtils.isEmailValid(email)) {
//                    inputEmail.setTextColor(fieldOk)
//                    true
//                } else {
//                    inputEmail.setTextColor(fieldNotOk)
//                    false
//                }
//                passCheck = if (pass.length > 5) {
//                    inputPass.setTextColor(fieldOk)
//                    true
//                } else {
//                    inputPass.setTextColor(fieldNotOk)
//                    false
//                }
//
//                loginButton.isEnabled = emailCheck && passCheck
//            }
//        }
//
//        inputEmail.addTextChangedListener(inputTextWatcher)
//        inputPass.addTextChangedListener(inputTextWatcher)
//
//        configureGoogleSignIn(this.requireContext())
//        loginGoogle.setOnClickListener {
//            signInWithGoogle()
//        }
//
//        observeData()
//
//        return myView
//    }
//
//    private fun observeData(){
//        firebaseViewModel.registerLiveData.observe(requireActivity(),{
//            if (it.resultOk) {
//                progressBar.visibility = View.INVISIBLE
//                Toast.makeText(context, getString(R.string.fr_log__login_success), Toast.LENGTH_LONG).show()
//                myView.findNavController().navigate(R.id.action_fragmentLogin_to_fragmentMain)
//            } else {
//                AlertDialog.Builder(activity)
//                    .setTitle("Alert")
//                    .setMessage(it.exception)
//                    .create()
//                    .show()
//            }
//            progressBar.visibility = View.INVISIBLE
//        })
//    }
//
//    private fun configureGoogleSignIn(context: Context){
//        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//            .requestIdToken(getString(com.firebase.ui.auth.R.string.default_web_client_id))
//            .requestEmail()
//            .build()
//        googleSignInClient = GoogleSignIn.getClient(context,gso)
//    }
//
//    private fun signInWithGoogle(){
//        val signInIntent = googleSignInClient.signInIntent
//        startActivityForResult(signInIntent, AppConstants.RC_SIGN_IN)
//    }
//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//
//        if (requestCode == AppConstants.RC_SIGN_IN) {
//            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
//            val exception = task.exception
//            if (task.isSuccessful) {
//                try {
//                    val account = task.getResult(ApiException::class.java)!!
//                    val credential = GoogleAuthProvider.getCredential(account.idToken,null)
//                    firebaseViewModel.actionLogin(credential)
//                } catch (e: ApiException) {
//                    AppUtils.createAlert(this.requireContext(),e.message.toString())
//                }
//            } else {
//                if (exception != null) {
//                    AppUtils.createAlert(this.requireContext(),exception.message.toString())
//                }
//            }
//        }
//    }

}