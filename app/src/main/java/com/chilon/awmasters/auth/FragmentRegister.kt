package com.chilon.awmasters.auth

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.chilon.awmasters.R
import com.chilon.awmasters.utilis.AppConstants
import com.chilon.awmasters.utilis.AppUtils
import com.chilon.awmasters.viewmodel.FirebaseViewModel
//import com.google.android.gms.auth.api.signin.GoogleSignIn
//import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.GoogleAuthProvider

class FragmentRegister : Fragment() {
//    private lateinit var progressBar: ProgressBar
//    private lateinit var inputEmail: TextInputEditText
//    private lateinit var inputEmailConfirm: EditText
//    private lateinit var inputPass: EditText
//    private lateinit var inputPassConfirm: EditText
//    private lateinit var buttonRegister: Button
//    private lateinit var buttonRegWithGoogle: LinearLayout
//    private lateinit var firebaseViewModel: FirebaseViewModel
//    private lateinit var myView: View
//    private lateinit var googleSignInClient: GoogleSignInClient
//
//    override fun onCreateView(
//        inflater: LayoutInflater, container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        myView = inflater.inflate(R.layout.fragment_register, container, false)
//
//        firebaseViewModel = ViewModelProvider(requireActivity()).get(FirebaseViewModel::class.java)
//
//        progressBar = myView.findViewById(R.id.fr_register__progress_bar)
//
//        inputEmail = myView.findViewById(R.id.fr_register__email_edit_text)
//        inputEmailConfirm = myView.findViewById(R.id.fr_register__email_confirm_edit_text)
//        inputPass = myView.findViewById(R.id.fr_register__pass_edit_text)
//        inputPassConfirm = myView.findViewById(R.id.fr_register__pass_confirm_edit_text)
//
//        buttonRegister = myView.findViewById(R.id.fr_register__btn_register)
//        buttonRegWithGoogle = myView.findViewById(R.id.fr_register__layout_google)
//
//        buttonRegister.setOnClickListener {
//            progressBar.visibility = View.VISIBLE
//
//            val credential = EmailAuthProvider.getCredential(
//                inputEmail.text.toString(),
//                inputPass.text.toString())
//            firebaseViewModel.actionRegister(credential)
//        }
//
//        configureGoogleSignIn(this.requireContext())
//        buttonRegWithGoogle.setOnClickListener {
//            signInWithGoogle()
//        }
//
//        val inputTextWatcher = object : TextWatcher {
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
//            override fun afterTextChanged(s: Editable?) {}
//
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//                val emailInp = inputEmail.text.toString().trim()
//                val emailInpConf = inputEmailConfirm.text.toString().trim()
//                val passInp = inputPass.text.toString().trim()
//                val passInpConf = inputPassConfirm.text.toString().trim()
//
//                val fieldOk = ContextCompat.getColor(myView.context, R.color.text_green)
//                val fieldNotOk = ContextCompat.getColor(myView.context, R.color.color_black)
//
//                val emailCheck: Boolean
//                val emailCheckConf: Boolean
//                val passCheck: Boolean
//                val passCheckConf: Boolean
//
//                emailCheck = if (AppUtils.isEmailValid(emailInp)) {
//                    inputEmail.setTextColor(fieldOk)
//                    true
//                } else {
//                    inputEmail.setTextColor(fieldNotOk)
//                    false
//                }
//                emailCheckConf =
//                    if (AppUtils.isEmailValid(emailInpConf) && emailInp == emailInpConf) {
//                        inputEmailConfirm.setTextColor(
//                            ContextCompat.getColor(
//                                myView.context,
//                                R.color.text_green
//                            )
//                        )
//                        true
//                    } else {
//                        inputEmailConfirm.setTextColor(fieldNotOk)
//                        false
//                    }
//                passCheck = if (passInp.length >= AppConstants.FIREBASE_AUTH_PASS_MIN_LENGTH) {
//                    inputPass.setTextColor(ContextCompat.getColor(myView.context, R.color.text_green))
//                    true
//                } else {
//                    inputPass.setTextColor(fieldNotOk)
//                    false
//                }
//                passCheckConf = if (passInp.length >= AppConstants.FIREBASE_AUTH_PASS_MIN_LENGTH &&
//                    passInp == passInpConf
//                ) {
//                    inputPassConfirm.setTextColor(
//                        ContextCompat.getColor(
//                            myView.context,
//                            R.color.text_green
//                        )
//                    )
//                    true
//                } else {
//                    inputPassConfirm.setTextColor(fieldNotOk)
//                    false
//                }
//
//                buttonRegister.isEnabled =
//                    emailCheck && emailCheckConf && passCheck && passCheckConf
//            }
//        }
//
//        inputEmail.addTextChangedListener(inputTextWatcher)
//        inputEmailConfirm.addTextChangedListener(inputTextWatcher)
//        inputPass.addTextChangedListener(inputTextWatcher)
//        inputPassConfirm.addTextChangedListener(inputTextWatcher)
//
//        observeData()
//
//        return myView
//    }
//
//    //Shared view model
//    private fun observeData() {
//        firebaseViewModel.registerLiveData.observe(requireActivity(), Observer {
//            if (it.resultOk) {
//                progressBar.visibility = View.INVISIBLE
//                Toast.makeText(myView.context, getString(R.string.fr_reg__account_created_ok), Toast.LENGTH_LONG).show()
//                myView.findNavController().navigate(R.id.action_fragmentRegister_to_fragmentMain)
//            } else {
//                AlertDialog.Builder(activity)
//                        .setTitle("Alert")
//                        .setMessage(it.exception)
//                        .create()
//                        .show()
//            }
//            progressBar.visibility = View.INVISIBLE
//        })
//    }
//
//    private fun configureGoogleSignIn(context:Context){
//        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//            .requestIdToken(getString(R.string.default_web_client_id))
//            .requestEmail()
//            .build()
//        googleSignInClient = GoogleSignIn.getClient(context,gso)
//    }
//
//    private fun signInWithGoogle(){
//        val signInIntent = googleSignInClient.signInIntent
//        startActivityForResult(signInIntent, AppConstants.RC_SIGN_IN)
//    }
//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//
//        if (requestCode == AppConstants.RC_SIGN_IN) {
//            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
//            val exception = task.exception
//            if (task.isSuccessful) {
//                try {
//                    val account = task.getResult(ApiException::class.java)!!
//                    val credential = GoogleAuthProvider.getCredential(account.idToken,null)
//                    firebaseViewModel.actionRegister(credential)
//                } catch (e: ApiException) {
//                    AppUtils.createAlert(this.requireContext(),e.message.toString())
//                }
//            } else {
//                if (exception != null) {
//                    AppUtils.createAlert(this.requireContext(),exception.message.toString())
//                }
//            }
//        }
//    }


}