package com.chilon.awmasters.ui

import android.content.ContentValues.TAG
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.InsetDrawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.chilon.awmasters.R


class SortMastersCustomDialogFragment(
    private val itemListener: OnButtonClickListener,
    private var languageList: ArrayList<String>,
    private var countryList: ArrayList<String>) : DialogFragment() {



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.dialog_sort_masters, container, false)
        val insetDrawable = InsetDrawable(ColorDrawable(Color.TRANSPARENT),30)
        dialog?.window?.setBackgroundDrawable(insetDrawable) //to see rounded corners and margins when small screen

        val radioGroupFilters = rootView.findViewById<RadioGroup>(R.id.radio_group_sort_masters)

        val spinnerLanguages = rootView.findViewById<Spinner>(R.id.dialog_sort_masters__spinner_languages)
        val spinnerAdapterLanguages: ArrayAdapter<String> =  object : ArrayAdapter<String>(rootView.context,
            androidx.appcompat.R.layout.support_simple_spinner_dropdown_item, languageList){
            override fun isEnabled(position: Int): Boolean {
                return position!=0
            }
        }
        spinnerLanguages.adapter = spinnerAdapterLanguages
        var selectedLanguage = ""

        val spinnerCountries = rootView.findViewById<Spinner>(R.id.dialog_sort_masters__spinner_countries)
        val spinnerAdapterCountires: ArrayAdapter<String> = object  : ArrayAdapter<String>(rootView.context,
            androidx.appcompat.R.layout.support_simple_spinner_dropdown_item, countryList){
            override fun isEnabled(position: Int): Boolean {
                return position!=0
            }
        }

        //val background:Drawable = ContextCompat.getDrawable(this.context,R.drawable.shape_round_corners_accent_16)
        spinnerCountries.adapter = spinnerAdapterCountires
        var selectedCountry = ""



        val onCheckedChangeListener =
            RadioGroup.OnCheckedChangeListener { _, checkedId ->
                if (checkedId != -1){
                    spinnerLanguages.setSelection(0)
                    selectedLanguage=""
                    spinnerCountries.setSelection(0)
                    selectedCountry=""
                }
            }

        radioGroupFilters.setOnCheckedChangeListener(onCheckedChangeListener)

        spinnerLanguages.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position!=0){
                    selectedLanguage = parent?.getItemAtPosition(position).toString()
                    radioGroupFilters.setOnCheckedChangeListener(null)
                    radioGroupFilters.clearCheck()
                    radioGroupFilters.setOnCheckedChangeListener(onCheckedChangeListener)
                    spinnerCountries.setSelection(0)
                    selectedCountry=""
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedLanguage = ""
            }
        }

        spinnerCountries.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position!=0){
                    selectedCountry = parent?.getItemAtPosition(position).toString()
                    radioGroupFilters.setOnCheckedChangeListener(null)
                    radioGroupFilters.clearCheck()
                    radioGroupFilters.setOnCheckedChangeListener(onCheckedChangeListener)
                    spinnerLanguages.setSelection(0)
                    selectedLanguage=""
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedCountry = ""
            }
        }


        rootView.findViewById<TextView>(R.id.dialog_sort_masters__button_ok).setOnClickListener {
            if (selectedLanguage!=""){
                itemListener.onGetSortLanguagesResultClick(selectedLanguage)
                dismiss()
            } else if (selectedCountry!="") {
                itemListener.onGetSortCountriesResultClick(selectedCountry)
                dismiss()
            } else {
                if (rootView.findViewById<RadioGroup>(R.id.radio_group_sort_masters).checkedRadioButtonId == -1) {
                    dismiss()
                } else {
                    val selectedID = rootView.findViewById<RadioGroup>(R.id.radio_group_sort_masters).checkedRadioButtonId
                    val selectedRadioButton = rootView.findViewById<RadioButton>(selectedID)
                    val selectedText = selectedRadioButton.text.toString()
                    itemListener.onGetSortRadioButtonResultClick(selectedText)
                    dismiss()
                }
            }
        }

        rootView.findViewById<TextView>(R.id.dialog_sort_masters__button_cancel).setOnClickListener {
            dismiss()
        }
        //rootView.background = this.context?.let { ContextCompat.getDrawable(it,R.drawable.shape_round_corners_accent_16) }
        return rootView
    }

    interface OnButtonClickListener {
        fun onGetSortRadioButtonResultClick(result: String)
        fun onGetSortLanguagesResultClick(result: String)
        fun onGetSortCountriesResultClick(result: String)
    }
}