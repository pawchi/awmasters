package com.chilon.awmasters.ui

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chilon.awmasters.R
import com.chilon.awmasters.data.Movie
import com.chilon.awmasters.data.MovieLocal
import com.chilon.awmasters.databinding.FragmentMoviesBinding
import com.chilon.awmasters.utilis.AppConstants
import com.chilon.awmasters.utilis.AppUtils
import com.chilon.awmasters.view.MoviesAdapter
import com.chilon.awmasters.viewmodel.FirebaseViewModel
import com.chilon.awmasters.viewmodel.MovieViewModel
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView


class FragmentMovies : Fragment(), MoviesAdapter.OnItemClickListener, SortMoviesDialogFragment.OnMovieSortClick {

    private lateinit var binding:FragmentMoviesBinding
    private var movieListFromFB: ArrayList<Movie> = ArrayList()
    private var movieListLocal: ArrayList<MovieLocal> = ArrayList()
    private var filteredMovieList: ArrayList<MovieLocal> = ArrayList()
    private lateinit var firebaseViewModel:FirebaseViewModel
    private lateinit var movieViewModel: MovieViewModel
    private lateinit var movieRecyclerView: RecyclerView
    private lateinit var linLayManager: LinearLayoutManager
    private lateinit var highlightSearchText: String
    private lateinit var tvNoContent: TextView
    private lateinit var progressBar: LinearLayout
    private var lastPosition: Int = 0
    private lateinit var adView: AdView
    private lateinit var adsLayout:LinearLayout
    private lateinit var closeCross:ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMoviesBinding.inflate(inflater,container,false)

        (activity as AppCompatActivity).supportActionBar?.title =
            getString(R.string.general__movies)

        firebaseViewModel = ViewModelProvider(this).get(FirebaseViewModel::class.java)
        movieViewModel = ViewModelProvider(this).get(MovieViewModel::class.java)

        adView = binding.moviesAdsBanner
        val adRequest: AdRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)

        adsLayout = binding.moviesAdsLayout
        closeCross = binding.moviesCloseAdCross
        closeCross.setOnClickListener {
            adsLayout.visibility = View.GONE
        }


        tvNoContent = binding.moviesTvNoContent
        highlightSearchText = ""

        progressBar = binding.loadMoviesLinLayoutProgressBar
        progressBar.visibility = View.VISIBLE

        movieRecyclerView = binding.recyclerMovies
        movieRecyclerView.layoutManager = LinearLayoutManager(this.context)
        linLayManager = (movieRecyclerView.layoutManager as LinearLayoutManager)
        movieRecyclerView.setHasFixedSize(false)

        lastPosition = AppUtils.getSharedPrefsInt(this.context, AppConstants.MOVIE_SCROLL_POS)
        getAllMovies()

        return binding.root
    }

    private fun getAllMovies(){
        firebaseViewModel.movieListLiveData.observe(viewLifecycleOwner){ fBList ->
            movieListFromFB=fBList
            movieListLocal = movieViewModel.getAllMoviesLocalOneTime() as ArrayList<MovieLocal>
            movieRecyclerView.adapter = MoviesAdapter(movieListLocal, this,highlightSearchText)
            updateRecycler()
            val howMuchMovies = resources.getText(R.string.fr_movie__movies_size).toString() + movieListLocal.size.toString()
            Toast.makeText(context,howMuchMovies,Toast.LENGTH_LONG).show()
        }
    }

    override fun movieToWatch(movie:MovieLocal) {
        if (movie.toWatch!=null){
            movie.toWatch = !movie.toWatch!!
        } else {
            movie.toWatch = true
        }
        movieViewModel.updateMovie(movie).apply {
            movieRecyclerView.adapter?.notifyDataSetChanged()
        }
    }

    override fun likesNumber(position: Int, movie: MovieLocal) {
        if (movie.favorite){
            movie.favorite = false
            movie.favLikesNo -=1
        } else {
            movie.favorite = true
            movie.favLikesNo +=1
        }
        firebaseViewModel.actionUpdateMovieLikesInFB(movie.title,movie.favLikesNo)
        movieViewModel.updateMovie(movie).apply {
            movieRecyclerView.adapter?.notifyItemChanged(position)
        }
//        if (AppUtils.userRegistered()){
//
//        } else {
//            Toast.makeText(context, "Only for registered users.", Toast.LENGTH_LONG).show()
//        }
    }

    override fun getRadioButtonResultText(rbText: String) {
        when (rbText){
            getString(R.string.movies__title) -> {
                movieListLocal.sortBy { it.title
                }
            }
            getString(R.string.movies__year) ->{
                movieListLocal.sortBy { it.year }
            }
            getString(R.string.pop_up_sorting__likes) -> {
                movieListLocal.sortBy { it.favLikesNo }
                movieListLocal.reverse()

            }

        }
        movieRecyclerView.adapter?.notifyDataSetChanged()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        val searchItem = menu.findItem(R.id.menu_home_action_search)
        searchItem.isVisible = true
        val searchView = searchItem?.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    val listToFilter: ArrayList<MovieLocal> = ArrayList()
                    listToFilter.addAll(movieListLocal)

                    filteredMovieList = listToFilter.filter { item ->
                        item.title.lowercase().contains(newText) ||
                                item.director.lowercase().contains(newText) ||
                                item.year.lowercase().contains(newText)
                    } as ArrayList<MovieLocal>
                    highlightSearchText = newText

                    if (filteredMovieList.size==0){
                        movieRecyclerView.visibility = View.GONE
                        tvNoContent.visibility = View.VISIBLE
                    } else {
                        movieRecyclerView.visibility = View.VISIBLE
                        tvNoContent.visibility = View.GONE
                    }
                    updateRecycler()
                }

                return false
            }
        })


        val sortItem = menu.findItem(R.id.menu_home_sort)
        sortItem.isVisible = true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_home_sort -> {
                activity?.let {
                    SortMoviesDialogFragment(this).show(
                        it.supportFragmentManager,
                        "custom dialog"
                    )
                }
            }
        }
        return true
    }

    override fun onPause() {
        super.onPause()
        lastPosition = linLayManager.findFirstVisibleItemPosition()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (filteredMovieList.size ==0){
            AppUtils.saveSharedPrefsInt(this.context,AppConstants.MOVIE_SCROLL_POS,lastPosition)
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState != null) {
            lastPosition = savedInstanceState.getInt(AppConstants.MOVIE_SCROLL_POS)
        }
    }

    fun updateRecycler(){
        if (filteredMovieList.size>0 && filteredMovieList.size<movieListLocal.size){
           movieRecyclerView.adapter = MoviesAdapter(filteredMovieList,this,highlightSearchText)
            movieRecyclerView.adapter?.notifyDataSetChanged()
        } else {
            movieRecyclerView.adapter = MoviesAdapter(movieListLocal,this,highlightSearchText)
            movieRecyclerView.adapter?.notifyDataSetChanged()
        }
        linLayManager.scrollToPosition(lastPosition)
        progressBar.visibility = View.GONE
    }

}