package com.chilon.awmasters.ui

import android.content.ContentValues
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.TextViewCompat
import androidx.navigation.Navigation
import com.chilon.awmasters.R
import com.chilon.awmasters.databinding.FragmentMainBinding
import com.chilon.awmasters.utilis.AppConstants
import com.chilon.awmasters.utilis.AppUtils
import com.chilon.awmasters.viewmodel.FirebaseViewModel
import com.google.android.gms.ads.*
import com.google.android.gms.ads.initialization.InitializationStatus
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback


class FragmentMain : Fragment() {

    private lateinit var binding: FragmentMainBinding
    private lateinit var firebaseViewModel: FirebaseViewModel
    private var mInterstitialAd: InterstitialAd? = null
    private var TAG = "FullScreen Ad in MainFragment "
    private var appJustStarted:Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainBinding.inflate(inflater,container,false)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.app_name)

        initializeAds()
        binding.frMainMasters.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.action_fragmentMain_to_fragment_show_masters)
        }

        binding.frMainQuotes.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.action_fragmentMain_to_fragmentQuotes)
        }

        binding.frMainMovies.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.action_fragmentMain_to_fragmentMovies)
        }
        return binding.root
    }

    private fun initializeAds(){
        appJustStarted = AppUtils.getSharedPrefsInt(context,AppConstants.SH_PREFS_MAIN_FR_SHOW_ADS_DELAY)
        this.context?.let {
            MobileAds.initialize(it, OnInitializationCompleteListener(){
                loadInterstitialAd()
//                if (AppUtils.getSharedPrefsInt(context,AppConstants.SH_PREFS_MAIN_FR_SHOW_ADS_DELAY) >1){
//                }
            })
        }
        appJustStarted++
        AppUtils.saveSharedPrefsInt(this.context, AppConstants.SH_PREFS_MAIN_FR_SHOW_ADS_DELAY, appJustStarted)
    }

    private fun loadInterstitialAd(){
        val adRequest = AdRequest.Builder().build()

        this.context?.let {
            InterstitialAd.load(it,"ca-app-pub-9832953507407797/5570103794", adRequest, object : InterstitialAdLoadCallback() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    adError?.toString()?.let { it1 -> Log.d(TAG, it1) }
                    mInterstitialAd = null
                }

                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    Log.d(TAG, "Ad was loaded.")
                    mInterstitialAd = interstitialAd
                    showInterstitialAd()
                }
            })
        }
    }

    private fun showInterstitialAd(){
        if (mInterstitialAd != null) {
            this.activity?.let { mInterstitialAd?.show(it) }
        } else {
            Log.d("TAG", "The interstitial ad wasn't ready yet.")
        }
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.frMainText1
        TextViewCompat.setAutoSizeTextTypeWithDefaults(binding.frMainText1,TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM)
        val minMasterTextSize: Double = TextViewCompat.getAutoSizeMinTextSize(binding.frMainText1)*0.8
        TextViewCompat.setAutoSizeTextTypeWithDefaults(binding.frMainText1, TextView.AUTO_SIZE_TEXT_TYPE_NONE)

        binding.frMainText1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, minMasterTextSize.toFloat())
        binding.frMainText2.setTextSize(TypedValue.COMPLEX_UNIT_DIP, minMasterTextSize.toFloat())
        binding.frMainText3.setTextSize(TypedValue.COMPLEX_UNIT_DIP, minMasterTextSize.toFloat())
    }

}