package com.chilon.awmasters.ui

import android.app.AlertDialog
import android.content.ContentValues.TAG
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.chilon.awmasters.R
import com.chilon.awmasters.data.Quote
import com.chilon.awmasters.utilis.AppConstants
import com.chilon.awmasters.view.QuotesAdapter
import com.chilon.awmasters.viewmodel.FirebaseViewModel
import com.chilon.awmasters.viewmodel.QuoteViewModel
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import java.util.*
import kotlin.collections.ArrayList

class FragmentQuotes : Fragment() {
    private lateinit var quotesRecycler: RecyclerView
    private lateinit var showQuoteButton: TextView
    private lateinit var firebaseViewModel: FirebaseViewModel
    private var listOfQuotesFbDB: ArrayList<Quote> = ArrayList()
    private lateinit var adView:AdView
    private lateinit var adsLayout:LinearLayout
    private lateinit var closeCross:ImageView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_quotes, container, false)
        (activity as AppCompatActivity).supportActionBar?.title =
            getString(R.string.toolbar_title__fr_quotes)

        firebaseViewModel = ViewModelProvider(this).get(FirebaseViewModel::class.java)

        adView = view.findViewById(R.id.quotes_ads_banner)
        val adRequest: AdRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)

        adsLayout = view.findViewById(R.id.quotes_ads_layout)
        closeCross = view.findViewById(R.id.quotes_close_ad_cross)
        closeCross.setOnClickListener {
            adsLayout.visibility = View.GONE
        }

        showQuoteButton = view.findViewById(R.id.quotes__show_quote_button)
        quotesRecycler = view.findViewById(R.id.quotes__recycler)

        quotesRecycler.layoutManager = LinearLayoutManager(this.context)
        LinearLayoutManager(this.context).reverseLayout = true

        getListFromFbDB()

        showQuoteButton.setOnClickListener {
            if (listOfQuotesFbDB.size>getQuotesListSizeFromSharedPrefs()){
                saveCurrentQuotesListSizeInSharedPref(getQuotesListSizeFromSharedPrefs() + 1)
                updateQuRecycler(getQuotesListToShowUser())
            } else {
                AlertDialog.Builder(context, R.style.MyStyleAlertDialog)
                    .setTitle(R.string.fr_quotes_alert_no_more_quotes)
                    .setMessage(getString(R.string.fr_quotes__no_more_quotes))
                    .setPositiveButton(
                        getString(R.string.general__yes),
                        DialogInterface.OnClickListener { dialogInterface, i ->
                            saveCurrentQuotesListSizeInSharedPref(1)
                            updateQuRecycler(getQuotesListToShowUser())
                        })
                    .setNegativeButton(
                        R.string.general__no,
                        DialogInterface.OnClickListener { dialogInterface, i -> })
                    .create()
                    .show()
            }
        }

        return view
    }

    private fun getListFromFbDB() {
        firebaseViewModel.quoteListLiveData.observe(viewLifecycleOwner) {
            listOfQuotesFbDB = it
            updateQuRecycler(getQuotesListToShowUser())
        }
    }

    private fun getQuotesListToShowUser(): ArrayList<Quote>{
        return ArrayList(listOfQuotesFbDB.take(getQuotesListSizeFromSharedPrefs()))
    }

    private fun updateQuRecycler(quotesList: ArrayList<Quote>){
        quotesList.reverse()
        quotesRecycler.adapter = QuotesAdapter(quotesList)
    }

    private fun getQuotesListSizeFromSharedPrefs(): Int {
        val sharedPref = activity?.getSharedPreferences(
            AppConstants.SHARED_PREFS_POOL,
            Context.MODE_PRIVATE
        )
        return sharedPref?.getInt(AppConstants.NUMBER_OF_DISPLAYED_QUOTES, 1) ?: 1
    }

    private fun saveCurrentQuotesListSizeInSharedPref(value: Int) {
        val sharedPref =
            activity?.getSharedPreferences(AppConstants.SHARED_PREFS_POOL, Context.MODE_PRIVATE)
        val editor = sharedPref?.edit()
        editor?.putInt(AppConstants.NUMBER_OF_DISPLAYED_QUOTES, value)
        editor?.apply()
    }
}