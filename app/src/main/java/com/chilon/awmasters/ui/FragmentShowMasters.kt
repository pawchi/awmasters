package com.chilon.awmasters.ui

import android.content.ContentValues.TAG
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chilon.awmasters.R
import com.chilon.awmasters.data.Master
import com.chilon.awmasters.data.MasterFav
import com.chilon.awmasters.utilis.AppConstants
import com.chilon.awmasters.utilis.AppUtils
import com.chilon.awmasters.view.ShowMastersAdapter
import com.chilon.awmasters.viewmodel.FirebaseViewModel
import com.chilon.awmasters.viewmodel.MasterFavViewModel
import com.google.android.gms.ads.*
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import kotlin.collections.ArrayList


class FragmentShowMasters() : Fragment(), ShowMastersAdapter.OnItemClickListener,
    SortMastersCustomDialogFragment.OnButtonClickListener {

    private var listOfMasters: ArrayList<Master> = ArrayList()
    private var listOfMastersFav: List<MasterFav> = emptyList()
    private var listOfMastersFiltered: ArrayList<Master> = ArrayList()
    private var listOfLanguages: ArrayList<String> = ArrayList()
    private var listOfCountries: ArrayList<String> = ArrayList()
    private lateinit var adView: AdView
    private lateinit var closeCross: ImageView
    private lateinit var adsLayout: LinearLayout
    private lateinit var myRecycler: RecyclerView
    private lateinit var myView: View
    private lateinit var linLayManager: LinearLayoutManager
    private var lastPosition: Int = 0
    private lateinit var firebaseViewModel: FirebaseViewModel
    private lateinit var masterFavViewModel: MasterFavViewModel
    private lateinit var highlightSearchText: String
    private lateinit var loadProgressBar: LinearLayout
    private lateinit var tvNoContent: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        retainInstance = true
        setHasOptionsMenu(true) //makes onCreateOptionsMenu accessible

        context?.let { MobileAds.initialize(it) }
        //loadFullScreenAd()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        myView = inflater.inflate(R.layout.fragment_show_masters, container, false)

        (activity as AppCompatActivity).supportActionBar?.title =
            getString(R.string.general__masters)

        firebaseViewModel = ViewModelProvider(this).get(FirebaseViewModel::class.java)
        masterFavViewModel = ViewModelProvider(this).get(MasterFavViewModel::class.java)
        loadProgressBar = myView.findViewById(R.id.load_masters_lin_layout)
        loadProgressBar.visibility = View.VISIBLE

        adView = myView.findViewById(R.id.show_masters_ads_banner)
        val adRequest: AdRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)

        adsLayout = myView.findViewById(R.id.show_masters_ads_layout)
        closeCross = myView.findViewById(R.id.show_masters_close_ad_cross)
        closeCross.setOnClickListener {
            adsLayout.visibility = View.GONE
        }


        myRecycler = myView.findViewById(R.id.recycler_show_masters)
        myRecycler.visibility = View.INVISIBLE
        myRecycler.setHasFixedSize(true)
        myRecycler.layoutManager = LinearLayoutManager(this.context)
        linLayManager = (myRecycler.layoutManager as LinearLayoutManager)

        tvNoContent = myView.findViewById(R.id.show_masters_tv_no_content)
        highlightSearchText = ""

        lastPosition = AppUtils.getSharedPrefsInt(this.context, AppConstants.MASTER_SCROLL_POS)

        getAllMasters()
        getAllMastersFav()

        return myView
    }


    @ExperimentalStdlibApi
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        val searchItem = menu.findItem(R.id.menu_home_action_search)
        searchItem.isVisible = true
        val searchView = searchItem?.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    val listToFilter: ArrayList<Master> = ArrayList()
                    listToFilter.addAll(listOfMasters)

                    listOfMastersFiltered = listToFilter.filter { item ->
                        item.nickName.lowercase().contains(newText) ||
                                item.primaryLanguage.lowercase().contains(newText) ||
                                item.countryOfResidence.lowercase().contains(newText) ||
                                item.countryOfBirth.lowercase().contains(newText) ||
                                item.surname.lowercase().contains(newText) ||
                                item.name.lowercase().contains(newText) ||
                                item.biography.lowercase().contains(newText)
                    } as ArrayList<Master>
                    highlightSearchText = newText

                    if (listOfMastersFiltered.size == 0) {
                        myRecycler.visibility = View.GONE
                        tvNoContent.visibility = View.VISIBLE
                    } else {
                        myRecycler.visibility = View.VISIBLE
                        tvNoContent.visibility = View.GONE
                        updateRecycler()
                    }
                }

                return false
            }
        })
        val sortItem = menu.findItem(R.id.menu_home_sort)
        sortItem.isVisible = true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_home_sort -> {
                val insetDrawable = InsetDrawable(ColorDrawable(Color.TRANSPARENT),10)
                activity?.let {
                    SortMastersCustomDialogFragment(
                        this,
                        getLanguages(listOfMasters),
                        getCountries(listOfMasters)
                    )
                        .show(it.supportFragmentManager, "custom dialog")
                }
            }
        }
        return true
    }

    private fun getLanguages(list: ArrayList<Master>): ArrayList<String> {
        listOfLanguages.clear()
        list.forEach { listItem ->
            var tempLanguageList: ArrayList<String> = ArrayList()
            if (listItem.primaryLanguage.contains(",")) {
                tempLanguageList = listItem.primaryLanguage.replace("\\s".toRegex(), "").toLowerCase().split(',') as ArrayList<String>
            } else {
                if (listItem.primaryLanguage != "null") {
                    tempLanguageList.add(listItem.primaryLanguage.toLowerCase().replace("\\s".toRegex(), ""))
                }
            }
            tempLanguageList.map { language ->
                language.replace("\\s".toRegex(), "")
                language.replaceFirstChar { it.lowercase() }
            }
            tempLanguageList.forEach { itemOfItemList ->
                val language = itemOfItemList.replace("\\s".toRegex(), "")
                if (!listOfLanguages.contains(language)) {
                    listOfLanguages.add(language)
                }
            }
        }
        if (!listOfLanguages.contains(getString(R.string.general__speaks_languages))) {
            listOfLanguages.add(0, getString(R.string.general__speaks_languages))
        }
        listOfLanguages.remove("")

        return listOfLanguages
    }

    private fun getCountries(list: ArrayList<Master>): ArrayList<String> {
        list.forEach { listItem ->
            var itemList: ArrayList<String> = ArrayList()
            if (listItem.countryOfResidence.contains(",")) {
                itemList = listItem.countryOfResidence.split(',') as ArrayList<String>
            } else {
                if (listItem.countryOfResidence != "null") {
                    itemList.add(listItem.countryOfResidence)
                }
            }

            itemList.forEach { itemOfItemList ->
                //val country = itemOfItemList.replace("\\s".toRegex(), "")
                if (!listOfCountries.contains(itemOfItemList)) {
                    listOfCountries.add(itemOfItemList)
                }
            }
        }
        if (!listOfCountries.contains(getString(R.string.general__lives_in))) {
            listOfCountries.add(0, getString(R.string.general__lives_in))
        }
        listOfCountries.remove("")
        return listOfCountries
    }

    private fun getAllMastersFav() {
        masterFavViewModel.getAllMastersFav().observe(viewLifecycleOwner) {
            listOfMastersFav = it
        }
    }

    private fun getAllMasters() {
        firebaseViewModel.masterListLiveData.observe(viewLifecycleOwner) {
            listOfMasters = it
            AppUtils.saveSharedPrefsInt(
                this.context,
                AppConstants.MASTER_LIST_SIZE,
                listOfMasters.size
            ) //for recognizing filtering action in adapter
            updateRecycler()
            val mastersSize =
                resources.getString(R.string.fr_show_mast__masters_size) + listOfMasters.size
            Toast.makeText(context, mastersSize, Toast.LENGTH_LONG).show()
        }
    }

    private fun updateRecycler() {
        if (listOfMastersFiltered.size > 0 && listOfMastersFiltered.size < listOfMasters.size) {
            myRecycler.adapter =
                ShowMastersAdapter(listOfMastersFiltered, this, highlightSearchText)
            myRecycler.adapter?.notifyDataSetChanged()
        } else {
            myRecycler.adapter = ShowMastersAdapter(listOfMasters, this, highlightSearchText)
            myRecycler.adapter?.notifyDataSetChanged()
        }
        (myRecycler.layoutManager as LinearLayoutManager).scrollToPosition(lastPosition)
        myRecycler.visibility = View.VISIBLE
        loadProgressBar.visibility = View.GONE
    }

    private fun recyclerScrollUp() {
        linLayManager.scrollToPosition(0)
    }

    override fun onPause() {
        super.onPause()
        lastPosition = linLayManager.findFirstVisibleItemPosition()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(AppConstants.SHOW_MASTER_RECYCLER_POSITION, lastPosition)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (listOfMastersFiltered.size == 0) {
            AppUtils.saveSharedPrefsInt(this.context, AppConstants.MASTER_SCROLL_POS, lastPosition)
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState != null) {
            lastPosition = savedInstanceState.getInt(AppConstants.SHOW_MASTER_RECYCLER_POSITION)
        }
    }

    override fun onFacebookClick(facebookLink: String) {
        if (facebookLink != "null" && facebookLink != "") {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(facebookLink))
            startActivity(browserIntent)
        } else {
            AppUtils.createAlert(requireActivity(), getString(R.string.fr_show_mast__no_facebook))
        }
    }

    override fun onYoutubeClick(youtubeLink: String) {
        if (youtubeLink != "null" && youtubeLink != "") {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(youtubeLink))
            startActivity(browserIntent)
        } else {
            AppUtils.createAlert(requireActivity(), getString(R.string.fr_show_mast__no_youtube))
        }

    }

    override fun onWwwClick(url: String) {
        var webPage = url
        if (url != "null" && url != "") {
            if (!url.startsWith("http://") && !url.startsWith("https://")) {
                webPage = "http://$url"
            }
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(webPage))
            startActivity(browserIntent)
        } else {
            AppUtils.createAlert(requireActivity(), getString(R.string.fr_show_mast__no_homepage))
        }
    }

    override fun onAddToFavoriteClick(position: Int, masterList: ArrayList<Master>) {
        val master = masterList[position]
        val masterNickname = master.nickName

        if (listOfMastersFav.any { it.nickName == masterNickname }) {
            masterFavViewModel.deleteMasterFavWithNickName(masterNickname)

            master.favorite = false
            val newLikesNumber: Long? = master.favLikesNo
            if (newLikesNumber != null) {
                firebaseViewModel.actionUpdateLikesInFB(master.docId, newLikesNumber)
            }
            myRecycler.adapter?.notifyItemChanged(position)

        } else {
            master.favorite = true
            val newLikesNumber: Long? = master.favLikesNo
            if (newLikesNumber != null) {
                firebaseViewModel.actionUpdateLikesInFB(master.docId, newLikesNumber)

            }
            masterFavViewModel.insertMasterFav(AppUtils.convertMasterToMasterFav(master))
            myRecycler.adapter?.notifyItemChanged(position)

        }
//        if (AppUtils.userRegistered()) {
//
//        } else {
//            Toast.makeText(context, "Only for registered users.", Toast.LENGTH_LONG).show()
//        }

    }

    override fun onEventsClick(events: String) {
        if (events != "null" && events != "") {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(events))
            startActivity(browserIntent)
        } else {
            AppUtils.createAlert(requireActivity(), getString(R.string.fr_show_mast__no_events))
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onGetSortRadioButtonResultClick(result: String) {
        when (result) {
            getString(R.string.pop_up_sorting__name) -> {
                listOfMasters.sortBy { list ->
                    list.nickName
                }
                lastPosition = 0
                updateRecycler()
                recyclerScrollUp()
            }
            getString(R.string.pop_up_sorting__likes) -> {
                listOfMasters.sortBy { list ->
                    list.favLikesNo
                }
                listOfMasters.reverse()
                lastPosition = 0
                updateRecycler()
                recyclerScrollUp()
            }
            getString(R.string.pop_up_sorting__age) -> {
                listOfMasters.sortBy { list ->
                    context?.let {
                        if (AppUtils.getAge(
                                list.dateOfBirth,
                                it
                            ) == getString(R.string.general__unknown)
                        ) {
                            150 //items with unknown date of birth show after calculated age
                            // calculated age should not exceed 150 years
                        } else if (list.dateOfDeath != getString(R.string.general__unknown)) {
                            200 //items deceased show at the end
                        } else {
                            AppUtils.getAge(list.dateOfBirth, it).toInt()
                        }
                    }
                }
                lastPosition = 0
                updateRecycler()
                recyclerScrollUp()
            }
        }
    }

    override fun onGetSortLanguagesResultClick(result: String) {
        listOfMasters.sortBy { it ->
            if (it.primaryLanguage.toLowerCase().contains(result)) {
                result
            } else {
                "zzz"
            }
        }
        updateRecycler()
        recyclerScrollUp()

    }

    override fun onGetSortCountriesResultClick(result: String) {
        listOfMasters.sortBy {
            if (it.countryOfResidence.contains(result)) {
                result
            } else {
                "zzz"
            }
        }
        updateRecycler()
        recyclerScrollUp()
    }

}