package com.chilon.awmasters.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chilon.awmasters.R
import com.chilon.awmasters.data.MasterFav
import com.chilon.awmasters.utilis.AppConstants
import com.chilon.awmasters.utilis.AppUtils
import com.chilon.awmasters.view.ShowMastersFavAdapter
import com.chilon.awmasters.viewmodel.FirebaseViewModel
import com.chilon.awmasters.viewmodel.MasterFavViewModel
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView


class FragmentShowMastersFav : Fragment(), ShowMastersFavAdapter.OnItemClickListener {

    private lateinit var masterFavViewModel: MasterFavViewModel
    private lateinit var recyclerViewFavMaster: RecyclerView
    private var listOfFavMasters: List<MasterFav> = emptyList()
    private lateinit var myView: View
    private var lastPosition: Int = 0
    private lateinit var firebaseViewModel: FirebaseViewModel
    private lateinit var linLayManager: LinearLayoutManager
    private lateinit var adView: AdView
    private lateinit var closeCross: ImageView
    private lateinit var adsLayout: LinearLayout

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        myView = inflater.inflate(R.layout.fragment_show_masters_fav, container, false)
        masterFavViewModel = ViewModelProvider(this).get(MasterFavViewModel::class.java)
        recyclerViewFavMaster = myView.findViewById(R.id.recycler_view_show_fav_masters)

        firebaseViewModel = ViewModelProvider(this).get(FirebaseViewModel::class.java)
        linLayManager = LinearLayoutManager(this.context)

        adView = myView.findViewById(R.id.show_masters_fav_ads_banner)
        val adRequest: AdRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)

        adsLayout = myView.findViewById(R.id.show_masters_fav_ads_layout)
        closeCross = myView.findViewById(R.id.show_masters_fav_close_ad_cross)
        closeCross.setOnClickListener {
            adsLayout.visibility = View.GONE
        }

        getAllFavMasters()

        return myView
    }

    private fun getAllFavMasters() {
        masterFavViewModel.getAllMastersFav().observe(viewLifecycleOwner, {
            if (it.isNotEmpty()) {
                listOfFavMasters = it
                updateRecycler()
            } else {
                recyclerViewFavMaster.adapter = null //needed when delete last item
            }
        })
    }

    private fun updateRecycler() {
        recyclerViewFavMaster.adapter = ShowMastersFavAdapter(listOfFavMasters, this)
        recyclerViewFavMaster.layoutManager = linLayManager
        (recyclerViewFavMaster.layoutManager as LinearLayoutManager).scrollToPosition(lastPosition)
    }

    override fun onRemoveFavClick(position: Int) {
        masterFavViewModel.deleteMasterFav(listOfFavMasters[position])
        val master = listOfFavMasters[position]
        val newLikesNumber: Long? = master.favLikesNo?.minus(1)
        if (newLikesNumber != null) {
            firebaseViewModel.actionUpdateLikesInFB(master.docId, newLikesNumber)
        }
        lastPosition =
            (recyclerViewFavMaster.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
    }

    override fun onFacebookClick(facebookLink: String) {
        if (facebookLink != "null") {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(facebookLink))
            startActivity(browserIntent)
        } else {
            AppUtils.createAlert(requireActivity(), getString(R.string.fr_show_mast__no_facebook))
        }
    }

    override fun onYoutubeClick(youtubeLink: String) {
        if (youtubeLink != "null") {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(youtubeLink))
            startActivity(browserIntent)
        } else {
            AppUtils.createAlert(requireActivity(), getString(R.string.fr_show_mast__no_youtube))
        }
    }

    override fun onWwwClick(url: String) {
        if (url != "null") {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(browserIntent)
        } else {
            AppUtils.createAlert(requireActivity(), getString(R.string.fr_show_mast__no_homepage))
        }
    }

    override fun onEventsClick(events: String) {
        if (events != "null") {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(events))
            startActivity(browserIntent)
        } else {
            AppUtils.createAlert(requireActivity(), getString(R.string.fr_show_mast__no_events))
        }
    }

    override fun onPause() {
        super.onPause()
        lastPosition = linLayManager.findFirstVisibleItemPosition()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(AppConstants.SHOW_MASTER_RECYCLER_POSITION, lastPosition)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState != null) {
            lastPosition = savedInstanceState.getInt(AppConstants.SHOW_MASTER_RECYCLER_POSITION)
        }
    }
}