package com.chilon.awmasters.ui

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.chilon.awmasters.R
import com.chilon.awmasters.databinding.FragmentAboutAppBinding
import com.chilon.awmasters.utilis.AppConstants


class FragmentAboutApp : Fragment() {

    private lateinit var binding: FragmentAboutAppBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentAboutAppBinding.inflate(inflater,container,false)

        binding.buttonRateAppLink.setOnClickListener {
            try {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=com.chilon.skoczeknew")
                    )
                )
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(this.context, e.message, Toast.LENGTH_LONG).show()
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(AppConstants.RATE_APP_URI)
                    )
                )
            }
        }

        return binding.root
    }


}