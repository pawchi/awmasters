package com.chilon.awmasters.ui

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chilon.awmasters.R
import com.chilon.awmasters.data.MovieLocal
import com.chilon.awmasters.view.MoviesFavAdapter
import com.chilon.awmasters.viewmodel.FirebaseViewModel
import com.chilon.awmasters.viewmodel.MovieViewModel


class FragmentMoviesToWatch : Fragment(), MoviesFavAdapter.OnItemClickListener, SortMoviesDialogFragment.OnMovieSortClick {

    private lateinit var movieViewModel: MovieViewModel
    private lateinit var firebaseViewModel: FirebaseViewModel
    private lateinit var movieRecyclerView: RecyclerView
    private var movieList:MutableList<MovieLocal> = ArrayList()
    private var filteredMovieList: ArrayList<MovieLocal> = ArrayList()
    private lateinit var highlightSearchText: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_movies_fav, container, false)

        (activity as AppCompatActivity).supportActionBar?.title =
            getString(R.string.toolbar_title__fr_movies_to_watch)

        movieViewModel = ViewModelProvider(this).get(MovieViewModel::class.java)
        firebaseViewModel = ViewModelProvider(this).get(FirebaseViewModel::class.java)

        highlightSearchText=""

        movieRecyclerView = view.findViewById(R.id.recycler_movies_to_watch)
        movieRecyclerView.layoutManager = LinearLayoutManager(this.context)
        movieRecyclerView.setHasFixedSize(false)
        getMovies()

        return view
    }

    private fun getMovies(){
        val tempMovieList = movieViewModel.getAllMoviesLocalOneTime() as ArrayList<MovieLocal>
        tempMovieList.forEach { movie ->
            if (movie.toWatch!=null){
                if (movie.toWatch==true){
                    movieList.add(movie)
                }
            }
        }
        movieRecyclerView.adapter = MoviesFavAdapter(movieList,this, highlightSearchText)
    }

    override fun getRadioButtonResultText(rbText: String) {
        when (rbText){
            getString(R.string.movies__title) -> {
                movieList.sortBy { it.title
                }
                movieRecyclerView.adapter?.notifyDataSetChanged()
            }
            getString(R.string.movies__year) ->{
                movieList.sortBy { it.year }
                movieRecyclerView.adapter?.notifyDataSetChanged()
            }
            getString(R.string.pop_up_sorting__likes) -> {
                movieList.sortBy { it.favLikesNo }
                movieList.reverse()
                movieRecyclerView.adapter?.notifyDataSetChanged()
            }

        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        val searchItem = menu.findItem(R.id.menu_home_action_search)
        searchItem.isVisible = true
        val searchView = searchItem?.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    val listToFilter: ArrayList<MovieLocal> = ArrayList()
                    listToFilter.addAll(movieList)

                    filteredMovieList = listToFilter.filter { item ->
                        item.title.lowercase().contains(newText)
                    } as ArrayList<MovieLocal>
                    highlightSearchText = newText
                }
                updateRecycler()
                return false
            }
        })

        val sortItem = menu.findItem(R.id.menu_home_sort)
        sortItem.isVisible = true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_home_sort -> {
                activity?.let {
                    SortMoviesDialogFragment(this).show(
                        it.supportFragmentManager,
                        "custom dialog"
                    )
                }
            }
        }
        return true
    }

    fun updateRecycler(){
        if (filteredMovieList.size>0 && filteredMovieList.size<movieList.size){
            movieRecyclerView.adapter = MoviesFavAdapter(filteredMovieList,this,highlightSearchText)
            movieRecyclerView.adapter?.notifyDataSetChanged()
        } else {
            movieRecyclerView.adapter = MoviesFavAdapter(movieList,this,highlightSearchText)
            movieRecyclerView.adapter?.notifyDataSetChanged()
        }
    }

    override fun movieToWatch(movie: MovieLocal) {
        movieList.remove(movie)
        movie.toWatch = false
        movieViewModel.updateMovie(movie)
        updateRecycler()
    }


}