package com.chilon.awmasters.ui

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.chilon.awmasters.R

class SortMoviesDialogFragment(private val rbResultListener: OnMovieSortClick) : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.dialog_sort_movies, container, false)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //to see rounded corners

        rootView.findViewById<TextView>(R.id.dialog_sort_movies__button_ok).setOnClickListener {
            if (rootView.findViewById<RadioGroup>(R.id.radio_group_sort_movies).checkedRadioButtonId == -1) {
                dismiss()
            } else {
                val rbId = rootView.findViewById<RadioGroup>(R.id.radio_group_sort_movies).checkedRadioButtonId
                val selectedRb = rootView.findViewById<RadioButton>(rbId)
                val selectedText = selectedRb.text.toString()
                rbResultListener.getRadioButtonResultText(selectedText)
                dismiss()
            }
        }

        rootView.findViewById<TextView>(R.id.dialog_sort_movies__button_cancel).setOnClickListener {
            dismiss()
        }

        return rootView
    }

    interface OnMovieSortClick{
        fun getRadioButtonResultText(rbText:String)
    }
}