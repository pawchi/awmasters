@file:Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")

package com.chilon.awmasters.utilis

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.ContentValues.TAG
import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.os.Build
import android.text.Spannable
import android.text.SpannableString
import android.text.style.BackgroundColorSpan
import android.util.Log
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.chilon.awmasters.R
import com.chilon.awmasters.data.*
import org.apache.commons.lang3.StringUtils
import org.apache.commons.text.StringEscapeUtils
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import kotlin.math.log


object AppUtils {

    @SuppressLint("SimpleDateFormat")
    @Throws(ParseException::class)
    fun getDateWithMonthAndDayInWords(currentDate: String?, context: Context): String? {
        var inputDateWithNoWhiteSpaces = currentDate?.replace("\\s".toRegex(), "")
        val inputForm = SimpleDateFormat("ddMMyyyy") // input form: '20210208'
        val outputForm = SimpleDateFormat("dd MMM yyyy") // output form: 'pon. 08 lut 2021'
        return if (inputDateWithNoWhiteSpaces.isNullOrEmpty() or inputDateWithNoWhiteSpaces.equals("null")) {
            context.getString(R.string.general__unknown)
        } else if (inputDateWithNoWhiteSpaces != null && inputDateWithNoWhiteSpaces.length < 5) {
            currentDate
        } else {
            val date = inputForm.parse(inputDateWithNoWhiteSpaces)
            outputForm.format(date)
        }

    }

    @SuppressLint("SimpleDateFormat")
    fun getAge(currentDate: String?, context: Context): String {
        val outputForm = SimpleDateFormat("yyyy") // input form: '20210208'
        val inputForm = SimpleDateFormat("dd MMM yyyy") // output form: 'pon. 08 lut 2021'
        var bornYear = context.getString(R.string.general__unknown)
        var result = context.getString(R.string.general__unknown)
        if (currentDate.isNullOrEmpty() or currentDate.equals("null") or currentDate.equals(
                context.getString(
                    R.string.general__unknown
                )
            )
        ) {
            bornYear = context.getString(R.string.general__unknown)
        } else {
            if (currentDate != null) {
                bornYear = if (currentDate.length == 4) {
                    currentDate
                } else {
                    val date = inputForm.parse(currentDate)
                    outputForm.format(date)
                }
            }
        }

        if (bornYear.length == 4) {
            val currentYear = Calendar.getInstance().get(Calendar.YEAR)
            result = (currentYear - bornYear.toInt()).toString()
        }
        return result
    }

    fun convertMasterToMasterFav(master: Master): MasterFav {
        return MasterFav(
            master.docId,
            master.nickName,
            master.name,
            master.surname,
            master.image,
            master.dateOfBirth,
            master.dateOfDeath,
            master.cityOfBirth,
            master.countryOfBirth,
            master.countryOfResidence,
            master.primaryLanguage,
            master.biography,
            master.homePage,
            master.youtube,
            master.facebook,
            master.events,
            master.favorite,
            master.favLikesNo
        )
    }

    fun isEmailValid(email: String): Boolean {
        val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }

    fun textContainsOnlyLettersAndNumbers(input: String): Boolean {
        val expression = "[a-zA-Z0-9]*"
        return input.matches(expression.toRegex())
    }

    fun createAlert(context: Context, message: String) {
        AlertDialog.Builder(context)
            .setTitle("Alert")
            .setMessage(message)
            .create()
            .show()
    }

    fun isNetworkConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val n: Network? = cm.activeNetwork
        if (n != null) {
            val nc = cm.getNetworkCapabilities(n)
            return nc!!.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || nc.hasTransport(
                NetworkCapabilities.TRANSPORT_WIFI
            )
        }
        return false
    }

    fun userRegistered(): Boolean {
        val currentUser = FirebaseManager.fbAuth.currentUser
        val providerType = currentUser?.getIdToken(false)?.result?.signInProvider
        return providerType == "password" || providerType == "google.com"

    }

    @ExperimentalStdlibApi
    fun setHighLightedText(tv: TextView, textToHighlight: String) {
        val tvt = tv.text.toString().lowercase()
        var ofe = tvt.indexOf(textToHighlight, 0)
        val wordToSpan: Spannable = SpannableString(tv.text)
        var ofs = 0
        while (ofs < tvt.length && ofe != -1) {
            ofe = tvt.indexOf(textToHighlight, ofs)
            if (ofe == -1) break else {
                // set color here
                wordToSpan.setSpan(
                    BackgroundColorSpan(
                        ContextCompat.getColor(
                            tv.context,
                            R.color.color_text_highlight
                        )
                    ),
                    ofe,
                    ofe + textToHighlight.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                tv.setText(wordToSpan, TextView.BufferType.SPANNABLE)
            }
            ofs = ofe + 1
        }
    }

    fun getItemWithLanguageSuffix(item: String): String {
        return when (Locale.getDefault().language) {
            "pl" -> item + "_pl"
            "de" -> item + "_ger"
            "es" -> item + "_esp"
            "hi" -> item + "_hin"
            else -> item + "_en"
        }
    }

    fun capitalizeFirstLetterOfEachWord(input: String): String {
        val words = input.split(" ").toMutableList()
        var output = ""
        for (word in words) {
            output += word.capitalize(Locale.getDefault()) + " "
        }
        return output.trim()
    }

    fun capitalizeFirstLetterOfEachWordAndAfterDash(input: String): String {
        val words = capitalizeFirstLetterOfEachWord(input).split(" ").toMutableList()
        for (i in words.indices) {
            if (words[i].contains("-")) {
                val parts = words[i].split("-")
                val capitalized = parts[0] + "-" + parts[1].capitalize()
                words[i] = capitalized
            }
        }
        return words.joinToString(separator = " ")
    }

    fun expandTextView(textView: TextView) {
        val animation: ObjectAnimator = ObjectAnimator.ofInt(textView, "maxLines", 80)
        animation.setDuration(500).start()
    }

    fun collapseTextView(textView: TextView) {
        val animation: ObjectAnimator = ObjectAnimator.ofInt(textView, "maxLines", 4)
        animation.setDuration(500).start()
    }

    fun convertMovieToMovieLocal(movie: Movie): MovieLocal {
        return MovieLocal(
            0,
            movie.title,
            movie.year,
            movie.posterUrl,
            movie.director,
            toWatch = null, favorite = false,
            favLikesNo = movie.favLikesNo
        )
    }

    fun getSharedPrefsInt(context: Context?, key: String): Int {
        val sharedPref = context?.getSharedPreferences(
            AppConstants.SHARED_PREFS_POOL,
            Context.MODE_PRIVATE
        )
        return sharedPref?.getInt(key, 1) ?: 1
    }

    fun saveSharedPrefsInt(context: Context?, key: String, value: Int) {
        val sharedPref =
            context?.getSharedPreferences(AppConstants.SHARED_PREFS_POOL, Context.MODE_PRIVATE)
        val editor = sharedPref?.edit()
        editor?.putInt(key, value)
        editor?.apply()
    }

    fun getLastXLettersFromText(howManyLetters: Int, text: String): String {
        return if (text.length > 9) {
            text.substring(text.length - howManyLetters)
        } else {
            text
        }
    }

    fun ifNoTextGetUnknown(context: Context, input: String): String {
        return if (input == "" || input == "null" || input==" ") context.getString(R.string.general__unknown) else input
    }

}