package com.chilon.awmasters.utilis

object AppConstants {

    //General
    val GOOGLE_SEARCH_API_KEY_FOR_AWMASTERS = "AIzaSyB23ENBfzt0HkmoJT2GdnJL35QZ5UwJoYM"
    val RATE_APP_URI = "https://play.google.com/store/apps/details?id=com.chilon.awmasters"
    const val FIREBASE_AUTH_PASS_MIN_LENGTH = 6
    const val RC_SIGN_IN = 9001
    //Main Fragment Menu
    const val FIRESTORE_MASTERS = "masters"
    const val FIRESTORE_QUOTES = "quotes"
    const val FIRESTORE_MOVIES = "movies"

    const val FRS_MASTER_NICKNAME = "nickName"
    const val FRS_MASTER_NAME ="name"
    const val FRS_MASTER_SURNAME ="surname"
    const val FRS_MASTER_IMAGE ="image"
    const val FRS_MASTER_DATE_OF_BIRTH ="dateOfBirth"
    const val FRS_MASTER_DATE_OF_DEATH ="dateOfDeath"
    const val FRS_MASTER_CITY_OF_BIRTH ="cityOfBirth"
    const val FRS_MASTER_COUNTRY_OF_BIRTH ="countryOfBirth"
    const val FRS_MASTER_COUNTRY_OF_RESIDANCE ="countryOfResidence"
    const val FRS_MASTER_PRIMARY_LANGUAGE ="primaryLanguage"
    const val FRS_MASTER_BIOGRAPHY ="biography"
    const val FRS_MASTER_HOMEPAGE ="homePage"
    const val FRS_MASTER_YOUTUBE ="youtube"
    const val FRS_MASTER_FACEBOOK ="facebook"
    const val FRS_MASTER_EVENTS = "events"
    const val FRS_MASTER_LIKES_COUNTER = "likesCounter"

    const val SHOW_MASTER_RECYCLER_POSITION = "smr_position"
    const val SHARED_PREFS_POOL = "shared_prefs"
    const val NUMBER_OF_DISPLAYED_QUOTES = "quotes_number"
    const val MASTER_LIST_SIZE = "master_list_size"
    const val MASTER_SCROLL_POS = "master_scroll_pos"

    const val FRS_QUOTES_SOURCE = "source"
    const val FRS_QUOTES_QUOTE = "quote"
    const val FRS_QUOTES_AUTHOR = "author"

    const val FRS_MOVIES_TITLE = "title"
    const val FRS_MOVIES_YEAR = "year"
    const val FRS_MOVIES_POSTER_URL = "posterUrl"
    const val FRS_MOVIES_LIKES_NO = "favLikesNo"
    const val FRS_MOVIES_DIRECTOR = "director"
    const val MOVIE_LIST_SIZE = "master_list_size"
    const val MOVIE_SCROLL_POS = "movie_scroll_pos"


    val SHOW_MASTER_SORT_RESULT = "master_sort_result"
    val SHOW_MASTER_SORT_NAME = "name"
    val SHOW_MASTER_SORT_COUNTRY = "country"
    val SHOW_MASTER_SORT_LANGUAGE = "language"
    val SHOW_MASTER_SORT_BIRTHDAY = "birthday"
    val SHOW_MASTER_SORT_MOSTLIKES = "mostlikes"

    val SHOW_MASTER_AGE_DECEASED = 200
    val SHOW_MASTER_AGE_UNKNOWN = 150


    //Shared Prefs
    val SH_PREFS_APP_STARTED_FIRST_TIME = "app_start_first_time"
    val SH_PREFS_BOOLEAN_DEFAULT_VALUE = true
    val SH_PREFS_DAY_OF_YEAR = "day_of_year"
    val SH_PREFS_NUMBER_OF_APP_OPENINGS_DEFAULT = 1
    val SH_PREFS_MAIN_FR_SHOW_ADS_DELAY = "delay_ads"


}