package com.chilon.awmasters

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.ContentValues.TAG
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.BillingClientStateListener
import com.android.billingclient.api.BillingResult
import com.android.billingclient.api.PurchasesUpdatedListener
import com.bumptech.glide.Glide
import com.chilon.awmasters.data.FirebaseManager
import com.chilon.awmasters.databinding.ActivityMainBinding
import com.chilon.awmasters.utilis.AppConstants
import com.chilon.awmasters.utilis.AppUtils
import com.chilon.awmasters.viewmodel.FirebaseViewModel
import com.google.android.gms.ads.*
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.google.firebase.Firebase
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestoreSettings

import java.util.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private lateinit var navHostFragment: NavHostFragment
    private lateinit var navigationView: NavigationView
    private lateinit var bottomNavView: BottomNavigationView
    private lateinit var firebaseViewModel: FirebaseViewModel

    private lateinit var menuItemRegister: MenuItem
    private lateinit var menuItemDelAccount: MenuItem
    private lateinit var bottomMenuItemFrMasters: MenuItem

    private lateinit var userName: TextView
    private lateinit var userPicture: ImageView

    private lateinit var billingClient:BillingClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        MobileAds.initialize(this)

        val toolbar: Toolbar = findViewById(R.id.toolbar)

        setSupportActionBar(toolbar) //Set app name in NoActionBar theme
        supportActionBar?.title = getString(R.string.app_name)
        val toggle = ActionBarDrawerToggle(
            this,
            binding.drawerLayout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        ) //add animated hamburger icon
        binding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.findNavController()

        navigationView = findViewById(R.id.navigation_view)
        navigationView.setNavigationItemSelectedListener(this)

        bottomNavView = findViewById(R.id.bottom_nav)
        bottomMenuItemFrMasters = bottomNavView.menu.findItem(R.id.masters_bottom_nav_home)
        bottomNavView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent))

        setUpFeaturesVisibilityInFragments(navController)
        setOnBottomNavItemSelected()
        //menuItemRegister = navigationView.menu.findItem(R.id.draw_menu__log_or_reg)
        //menuItemDelAccount = navigationView.menu.findItem(R.id.draw_menu__remove_account)

        firebaseViewModel = ViewModelProvider(this).get(FirebaseViewModel::class.java)
        //observeData()


        val settings = firestoreSettings {
            isPersistenceEnabled = true
        }
        FirebaseFirestore.getInstance().firestoreSettings = settings

//        val headerView = navigationView.getHeaderView(0)
//        userName = headerView.findViewById(R.id.header_user_name)
//        userPicture = headerView.findViewById(R.id.header_user_image)


        if (FirebaseManager.fbAuth.currentUser == null){
            FirebaseManager.fbAuth.signInAnonymously()
            Log.d("User null", ": ********************** null.")
        } else {
            Log.d("User logged in", ": ********************** NOT null.")
        }

//        if (FirebaseManager.fbAuth.currentUser == null || FirebaseManager.fbAuth.currentUser!!.isAnonymous) {
//            Log.d("User logged in", ": ********************** anonymously.")
//            FirebaseManager.fbAuth.signInAnonymously().addOnCompleteListener {
//                Log.d("User logged in", ": ********************** anonymously.")
//            }
//        } else {
//            Log.d("User logged in", ": ********************** anonymously.")
//        }
        //initializeBillingClient()

    }


//    private fun firebaseMessagingRetrieveCurrentToken(){
//        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
//            if (!task.isSuccessful) {
//                Log.w(TAG, "Fetching FCM registration token failed", task.exception)
//                return@OnCompleteListener
//            }
//
//            // Get new FCM registration token
//            val token = task.result
//
//            // Log and toast
//
//            Log.d(TAG, "token **************************token: $token")
//            Toast.makeText(baseContext, "Device registration token: $token", Toast.LENGTH_SHORT).show()
//        })
//    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val homeIcon = menu?.findItem(R.id.home_button)
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.home_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home_button -> {
                bottomMenuItemFrMasters.setChecked(true)
                navController.navigate(R.id.fragmentMain)
            }
        }
        return super.onOptionsItemSelected(item)
    }


    //Shared view model
//    private fun observeData() {
//        firebaseViewModel.registerLiveData.observe(this, Observer {
//            if (it.resultOk) {
//                updateUIForRegisteredUsers()
//            } else {
//                updateUIForNonRegistered()
//            }
//        })
//
//        firebaseViewModel.removeAccountLiveData.observe(this) { authResult ->
//            if (authResult.resultOk) {
//                Toast.makeText(
//                    applicationContext,
//                    "Account successfully removed.",
//                    Toast.LENGTH_SHORT
//                )
//                    .show()
//                updateUIForNonRegistered()
//                recreateActivity()
//            } else {
//                if (authResult.exception == application.getString(R.string.remove_account__require_log_in)) {
//                    val view =
//                        LayoutInflater.from(this).inflate(R.layout.alert_input_email_and_pass, null)
//                    val inpEmailEditText = view.findViewById<EditText>(R.id.alert_inp_email)
//                    val inpPassEditText = view.findViewById<EditText>(R.id.alert_inp_pass)
//                    val message = authResult.exception
//                    FirebaseManager.fbAuth.currentUser?.providerData?.forEach {
//                        if (it.providerId == "google.com") {
//                            AlertDialog.Builder(this, R.style.MyStyleAlertDialog)
//                                .setMessage(message)
//                                .setPositiveButton(application.getString(R.string.general__relogin_and_delete)) { dialogInterface: DialogInterface, i: Int ->
//                                    //signInWithGoogleAndDelete()
//                                }
//                                .setNegativeButton(application.getString(R.string.general__cancel)) { dialogInterface: DialogInterface, i: Int -> }
//                                .create()
//                                .show()
//                        }
//                        if (it.providerId == "password") {
//                            val alertDialog = AlertDialog.Builder(this)
//                                .setMessage(message)
//                                .setView(view)
//                                .setPositiveButton(application.getString(R.string.general__delete)) { dialogInterface: DialogInterface, i: Int ->
//                                    reloginUserAndDeleteAccount(
//                                        inpEmailEditText.text.toString(),
//                                        inpPassEditText.text.toString()
//                                    )
//                                }
//                                .setNegativeButton(application.getString(R.string.general__cancel)) { dialogInterface: DialogInterface, i: Int -> }
//                                .create()
//
//                            alertDialog.setOnShowListener{
//                                alertDialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_NEGATIVE).setTextColor(getColor(R.color.color_braun))
//                                alertDialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_POSITIVE).setTextColor(getColor(R.color.color_braun))
//                            }
//                            alertDialog.show()
//                        }
//                    }
//
//                } else {
//                    createAlertDialog(authResult.exception)
//                }
//            }
//        }
//    }

//    private fun recreateActivity() {
//        finish()
//        overridePendingTransition(0, 0)
//        startActivity(intent)
//        overridePendingTransition(0, 0)
//    }
//
//    private fun reloginWithGoogleAndDeleteAccount(idToken: String?) {
//        val googleAccount = GoogleSignIn.getLastSignedInAccount(this)
//        if (googleAccount != null) {
//            val credential = GoogleAuthProvider.getCredential(idToken, null)
//            FirebaseManager.fbAuth.currentUser?.reauthenticate(credential)?.addOnCompleteListener {
//                if (it.isSuccessful) {
//                    firebaseViewModel.actionRemoveUserAccount()
//                } else {
//                    if (it.exception?.message?.isNotEmpty() == true) {
//                        it.exception!!.message?.let { it1 -> createAlertDialog(it1) }
//                    } else {
//                        createAlertDialog(getString(R.string.remove_account__relogin_failed))
//                    }
//                }
//            }
//        }
//
//    }

//    private fun signInWithGoogleAndDelete() {
//        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//            .requestIdToken(getString(R.string.default_web_client_id))
//            .requestEmail()
//            .build()
//        val signInIntent = GoogleSignIn.getClient(this, gso).signInIntent
//        startActivityForResult(signInIntent, AppConstants.RC_SIGN_IN)
//    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode == AppConstants.RC_SIGN_IN) {
//            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
//            val exception = task.exception
//            if (task.isSuccessful) {
//                try {
//                    val account = task.getResult(ApiException::class.java)!!
//                    Log.d("SignInActivity", "firebaseAuthWithGoogle:" + account.id)
//                    //firebaseViewModel.actionLoginWithGoogle(account.idToken!!)
//                    reloginWithGoogleAndDeleteAccount(account.idToken)
//                } catch (e: ApiException) {
//                    Log.w("SignInActivity", "Google sign in failed", e)
//                    AppUtils.createAlert(this, e.message.toString())
//                }
//            } else {
//                Log.w("SignInActivity", exception.toString())
//                if (exception != null) {
//                    AppUtils.createAlert(this, exception.message.toString())
//                }
//            }
//        }
//    }

//    private fun reloginUserAndDeleteAccount(email: String, pass: String) {
//        val credential = EmailAuthProvider.getCredential(email, pass)
//        FirebaseManager.fbAuth.currentUser?.reauthenticate(credential)?.addOnCompleteListener {
//            if (it.isSuccessful) {
//                firebaseViewModel.actionRemoveUserAccount()
//            } else {
//                if (it.exception?.message?.isNotEmpty() == true) {
//                    it.exception!!.message?.let { it1 -> createAlertDialog(it1) }
//                } else {
//                    createAlertDialog(getString(R.string.remove_account__relogin_failed))
//                }
//            }
//        }
//    }

//    @SuppressLint("UseCompatLoadingForDrawables")
//    private fun updateUIForRegisteredUsers() {
//        menuItemRegister.isVisible = false
//        menuItemDelAccount.isVisible = true
//        var currentUserPictureUrl = ""
//        var currentUserName = ""
//
//        val curUser = FirebaseManager.fbAuth.currentUser
//        curUser?.providerData?.forEach {
//            if (it.providerId == "google.com") {
//                currentUserPictureUrl = it.photoUrl.toString()
//                currentUserName = it.displayName.toString()
//            } else if (it.providerId == "password") {
//                currentUserName = it.email.toString()
//                if (it.photoUrl != null) {
//                    currentUserPictureUrl = it.photoUrl.toString()
//                }
//                currentUserName = it.email.toString()
//            }
//        }
//
//        userName.text = currentUserName
//        if (currentUserPictureUrl != "") {
//            Glide.with(applicationContext)
//                .load(currentUserPictureUrl)
//                .circleCrop()
//                .into(userPicture)
//        }
//
//    }

//    private fun updateUIForNonRegistered() {
//        menuItemRegister.isVisible = true
//        menuItemDelAccount.isVisible = false
//        userName.text = getString(R.string.general__user_anonymous)
//    }

    override fun onStart() {
        super.onStart()
//        val currentUser = FirebaseManager.fbAuth.currentUser
//        val dayOfYear = Calendar.getInstance().get(Calendar.DAY_OF_YEAR)
//
//        if (getPreferences(Context.MODE_PRIVATE).getBoolean(
//                AppConstants.SH_PREFS_APP_STARTED_FIRST_TIME,
//                AppConstants.SH_PREFS_BOOLEAN_DEFAULT_VALUE
//            )
//        ) {
//            getPreferences(Context.MODE_PRIVATE).edit()
//                .putBoolean(AppConstants.SH_PREFS_APP_STARTED_FIRST_TIME, false).apply()
//            getPreferences(Context.MODE_PRIVATE).edit()
//                .putInt(AppConstants.SH_PREFS_DAY_OF_YEAR, dayOfYear).apply()
//            navController.navigate(R.id.fragmentLoginOrRegister)
//        } else {
//            if (currentUser != null) {
//                currentUser.reload().addOnSuccessListener {
//
//                    if (AppUtils.userRegistered()) {
//                        updateUIForRegisteredUsers()
//                    } else {
//                        updateUIForNonRegistered()
//                    }
//                }
//            } else {
//                //Remind registering once a day
//                val lastShownRegisterReminderDayOfYear = getPreferences(Context.MODE_PRIVATE)
//                    .getInt(AppConstants.SH_PREFS_DAY_OF_YEAR, 0)
//
//                if (dayOfYear > lastShownRegisterReminderDayOfYear) {
//                    getPreferences(Context.MODE_PRIVATE).edit()
//                        .putInt(AppConstants.SH_PREFS_DAY_OF_YEAR, dayOfYear).apply()
//                    navController.navigate(R.id.fragmentLoginOrRegister)
//                }
//
//                updateUIForNonRegistered()
//            }
//        }
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    private fun setUpFeaturesVisibilityInFragments(controller: NavController) {
        controller.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.fragment_show_masters -> showInMastersFragments()
                R.id.fragment_show_masters_fav -> showInMastersFragments()
                R.id.fragmentMovies -> showInMoviesFragments()
                R.id.fragmentMoviesFav -> showInMoviesFragments()
                else -> hideInFragment()
            }
        }
    }

    private fun showInMoviesFragments() {
        binding.bottomNav.visibility = View.VISIBLE
        binding.bottomNav.menu.findItem(R.id.masters_bottom_nav_home).isVisible = false
        binding.bottomNav.menu.findItem(R.id.masters_bottom_nav_fav).isVisible = false
        binding.bottomNav.menu.findItem(R.id.movies_bottom_nav_home).isVisible = true
        binding.bottomNav.menu.findItem(R.id.movies_bottom_nav_fav).isVisible = true
        binding.bottomNav.menu.findItem(R.id.movies_bottom_nav_home).isChecked = true
    }

    private fun showInMastersFragments() {
        binding.bottomNav.visibility = View.VISIBLE
        binding.bottomNav.menu.findItem(R.id.masters_bottom_nav_home).isVisible = true
        binding.bottomNav.menu.findItem(R.id.masters_bottom_nav_fav).isVisible = true
        binding.bottomNav.menu.findItem(R.id.movies_bottom_nav_home).isVisible = false
        binding.bottomNav.menu.findItem(R.id.movies_bottom_nav_fav).isVisible = false
    }

    private fun hideInFragment() {
        binding.bottomNav.visibility = View.GONE
        //activity_main__search_constraint_layout.visibility = View.GONE
    }

    private fun setOnBottomNavItemSelected() {
        binding.bottomNav.setOnItemSelectedListener {

            when (it.itemId) {
                R.id.masters_bottom_nav_home -> {
                    Navigation.findNavController(navHostFragment.requireView())
                        .navigate(R.id.glob_action__move_to_fr_show_masters)
                }
                R.id.masters_bottom_nav_fav -> {
                    Navigation.findNavController(navHostFragment.requireView())
                        .navigate(R.id.glob_action__move_to_fr_show_fav_masters)
                }
                R.id.movies_bottom_nav_home -> {
                    Navigation.findNavController(navHostFragment.requireView())
                        .navigate(R.id.glob_action__move_to_fr_movies)
                }
                R.id.movies_bottom_nav_fav -> {
                    Navigation.findNavController(navHostFragment.requireView())
                        .navigate(R.id.glob_action__move_to_fr_movies_fav)
                }


            }
            true
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
//            R.id.draw_menu__log_or_reg -> Navigation.findNavController(navHostFragment.requireView())
//                .navigate(R.id.fragmentLoginOrRegister)
            R.id.draw_menu__rate -> rateApp()
            R.id.draw_menu__invite_friend -> shareWithFriends("Link do aplikacji")
//            R.id.draw_menu__remove_account ->
//                AlertDialog.Builder(this, R.style.MyStyleAlertDialog)
//                    .setMessage(R.string.remove_account__confirm)
//                    .setPositiveButton("OK") { dialogInterface: DialogInterface, i: Int ->
//                        firebaseViewModel.actionRemoveUserAccount()
//                    }
//                    .setNegativeButton("Cancel") { dialogInterface: DialogInterface, i: Int -> }
//                    .create()
//                    .show()


//            R.id.draw_menu__remove_ads -> Toast.makeText(
//                applicationContext,
//                "${item.itemId} TODO - REMOVE ADS ACTION",
//                Toast.LENGTH_SHORT
//            ).show()

            R.id.draw_menu__about_app -> Navigation.findNavController(navHostFragment.requireView())
                .navigate(R.id.fragmentAboutApp)
        }
        binding.drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

//    private fun initializeBillingClient(){
//         val purchasesUpdatedListener =
//            PurchasesUpdatedListener { billingResult, purchases ->
//                // To be implemented in a later section.
//            }
//
//        billingClient = BillingClient.newBuilder(applicationContext)
//            .setListener(purchasesUpdatedListener)
//            .enablePendingPurchases()
//            .build()
//    }

//    private fun connectGooglePlayBilling(){
//        billingClient.startConnection(object : BillingClientStateListener {
//            override fun onBillingSetupFinished(billingResult: BillingResult) {
//                if (billingResult.responseCode ==  BillingClient.BillingResponseCode.OK) {
//                    // The BillingClient is ready. You can query purchases here.
//                }
//            }
//            override fun onBillingServiceDisconnected() {
//                // Try to restart the connection on the next request to
//                // Google Play by calling the startConnection() method.
//            }
//        })
//    }

    private fun getProduct(){

    }

    override fun onStop() {
        super.onStop()
        AppUtils.saveSharedPrefsInt(this,AppConstants.SH_PREFS_MAIN_FR_SHOW_ADS_DELAY, 0)
        AppUtils.saveSharedPrefsInt(this, AppConstants.MASTER_SCROLL_POS, 0)
        AppUtils.saveSharedPrefsInt(this, AppConstants.MOVIE_SCROLL_POS, 0)
    }

    fun shareWithFriends(message: String) {
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, message)
            type = "text/plain"
        }
        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)
    }

    private fun rateApp() {
        try {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(AppConstants.RATE_APP_URI)
                )
            )
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
        }
    }

    private fun createAlertDialog(message: String) {
        AlertDialog.Builder(this)
            .setMessage(message)
            .create()
            .show()

    }
}