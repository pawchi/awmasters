package com.chilon.awmasters.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import com.chilon.awmasters.data.MovieDataBase
import com.chilon.awmasters.data.MovieLocal
import com.chilon.awmasters.data.MovieLocalDao
import kotlinx.coroutines.*

class LocalMovieRepository(application: Application) {
    private var movieLocalDao: MovieLocalDao

    init {
        val movieDataBase = MovieDataBase.getInstance(application)!!
        movieLocalDao = movieDataBase.movieLocalDao()
    }

    fun insertMovieLocal(movieLocal: MovieLocal): Job = CoroutineScope(Dispatchers.Default).launch {
        movieLocalDao.insertMovieLocal(movieLocal)
    }

    fun updateMovieLocal(movieLocal: MovieLocal): Job {
        return CoroutineScope(Dispatchers.Default).launch {
            movieLocalDao.updateMovieLocal(movieLocal)
        }
    }

    fun getAllMoviesLocal(): Deferred<LiveData<List<MovieLocal>>> =
        CoroutineScope(Dispatchers.Default).async {
            movieLocalDao.getAllMoviesLocal()
        }

    fun getAllMoviesLocalOneTime(): Deferred<List<MovieLocal>> =
        CoroutineScope(Dispatchers.Default).async {
            movieLocalDao.getAllMoviesLocalOneTime()
        }

}