package com.chilon.awmasters.repository

import android.app.Application
import android.content.ContentValues.TAG
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import com.chilon.awmasters.data.MasterFav
import com.chilon.awmasters.data.Quote
import com.chilon.awmasters.data.QuoteDao
import com.chilon.awmasters.data.QuoteDataBase
import kotlinx.coroutines.*
import java.lang.Exception

class LocalQuoteRepository(application: Application) {
    private var quoteDao:QuoteDao

    init {
        val quoteDataBase: QuoteDataBase = QuoteDataBase.getInstance(application)!!
        quoteDao = quoteDataBase.quoteDao()
    }

    fun insertQuote(quote: Quote): Job = CoroutineScope(Dispatchers.Default).launch {
        quoteDao.insertQuote(quote)
    }

    fun getAllQuotes(): Deferred<LiveData<List<Quote>>> =
        CoroutineScope(Dispatchers.Default).async {
            quoteDao.getAllQuotes()
        }
}