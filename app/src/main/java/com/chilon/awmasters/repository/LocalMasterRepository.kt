package com.chilon.awmasters.repository

import android.app.Application
import androidx.lifecycle.LiveData
import com.chilon.awmasters.data.Master
import com.chilon.awmasters.data.MasterFav
import com.chilon.awmasters.data.MasterFavDataBase
import com.chilon.awmasters.data.MasterFavDao
import kotlinx.coroutines.*

class LocalMasterRepository(application: Application) {
    private var masterFavDao: MasterFavDao


    init {
        val favDataBase: MasterFavDataBase = MasterFavDataBase.getInstance(application)!!
        masterFavDao = favDataBase.masterFavDao()

    }

    fun insertMasterFav(masterFav: MasterFav): Job = CoroutineScope(Dispatchers.Default).launch {
        masterFavDao.insertMasterFav(masterFav)
    }

    fun updateMasterFav(masterFav: MasterFav): Job {
        return CoroutineScope(Dispatchers.Default).launch {
            masterFavDao.updateMasterFav(masterFav)
        }
    }

    fun deleteMasterFav(masterFav: MasterFav): Job = CoroutineScope(Dispatchers.Default).launch {
        masterFavDao.deleteMasterFav(masterFav)
    }

    fun deleteMasterFavWithNickName(nickName: String): Job = CoroutineScope(Dispatchers.Default).launch {
        masterFavDao.deleteMasterFavWithNickName(nickName)
    }

    fun deleteAllMasterFav() {
        CoroutineScope(Dispatchers.Default).launch {
            masterFavDao.deleteAllMasterFav()
        }
    }

    fun getMasterFavWithNickName(nickName:String): Deferred<LiveData<List<MasterFav>>> =
        CoroutineScope(Dispatchers.Default).async {
            masterFavDao.getMasterFavWithNickName(nickName)
        }

    fun getAllMasterFav(): Deferred<LiveData<List<MasterFav>>> =
        CoroutineScope(Dispatchers.Default).async {
            masterFavDao.getAllMastersFav()
        }
}