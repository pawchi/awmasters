package com.chilon.awmasters.repository

import android.app.Application
import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.chilon.awmasters.R
import com.chilon.awmasters.data.*
import com.chilon.awmasters.utilis.AppConstants
import com.chilon.awmasters.utilis.AppUtils
import com.chilon.awmasters.utilis.FbAuthResult
import com.google.android.gms.tasks.Task
import com.google.android.material.tabs.TabLayout
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.*
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception
import kotlin.collections.ArrayList

class FirebaseRepository(private val application: Application) {

    private val firestoreDB = FirebaseFirestore.getInstance()
    val fBRepoMasterListLiveData = MutableLiveData<ArrayList<Master>>()
    val fBRepoRegisterFbLiveData = MutableLiveData<FbAuthResult>()
    val fBRepoDeleteFbUserLiveData = MutableLiveData<FbAuthResult>()
    val fBRepoMovieListLiveData = MutableLiveData<ArrayList<Movie>>()
    val fBRepoQuoteListLiveData = MutableLiveData<ArrayList<Quote>>()


    fun getAllMasters() {
        val listOfMasters = ArrayList<Master>()
        firestoreDB.collection(AppConstants.FIRESTORE_MASTERS)
            .get()
            .addOnSuccessListener { documents ->
//                if (e != null) {
//                    Log.w(TAG, "Listen Masters failed.", e)
//                }
                if (documents != null) {
                    for (document in documents) {
                        val master = Master(
                            document.id,
                            document.get(AppConstants.FRS_MASTER_NICKNAME).toString(),
                            document.get(AppConstants.FRS_MASTER_NAME).toString(),
                            document.get(AppConstants.FRS_MASTER_SURNAME).toString(),
                            document.get(AppConstants.FRS_MASTER_IMAGE).toString(),
                            AppUtils.getDateWithMonthAndDayInWords(
                                document.get(AppConstants.FRS_MASTER_DATE_OF_BIRTH).toString(),
                                application.baseContext
                            ),
                            AppUtils.getDateWithMonthAndDayInWords(
                                document.get(AppConstants.FRS_MASTER_DATE_OF_DEATH).toString(),
                                application.baseContext
                            ),
                            document.get(AppConstants.FRS_MASTER_CITY_OF_BIRTH).toString(),
                            document.get(AppConstants.FRS_MASTER_COUNTRY_OF_BIRTH).toString(),
                            document.get(AppConstants.FRS_MASTER_COUNTRY_OF_RESIDANCE).toString(),
                            document.get(AppConstants.FRS_MASTER_PRIMARY_LANGUAGE).toString(),
                            document.get(AppUtils.getItemWithLanguageSuffix(AppConstants.FRS_MASTER_BIOGRAPHY))
                                .toString(),
                            document.get(AppConstants.FRS_MASTER_HOMEPAGE).toString(),
                            document.get(AppConstants.FRS_MASTER_YOUTUBE).toString(),
                            document.get(AppConstants.FRS_MASTER_FACEBOOK).toString(),
                            document.get(AppConstants.FRS_MASTER_EVENTS).toString(),
                            false,
                            if (document.getLong(AppConstants.FRS_MASTER_LIKES_COUNTER) != null) {
                                if (document.getLong(AppConstants.FRS_MASTER_LIKES_COUNTER)
                                        .toString() == "null"
                                ) {
                                    0
                                } else {
                                    document.getLong(AppConstants.FRS_MASTER_LIKES_COUNTER)
                                }
                            } else {
                                0
                            }
                        )
                        listOfMasters.add(master)
                    }
                }
                val favDataBase: MasterFavDataBase = MasterFavDataBase.getInstance(application)!!
                val masterFavDao = favDataBase.masterFavDao()
                CoroutineScope(Dispatchers.Default).launch {
                    listOfMasters.forEach {
                        val isFavList = masterFavDao.getFav(it.nickName)
                        it.favorite = isFavList.isNotEmpty()
                        val itemFavMaster = AppUtils.convertMasterToMasterFav(it)

                        if (it.favorite) {
                            itemFavMaster.masterFavId = isFavList[0].masterFavId
                            masterFavDao.updateMasterFav(itemFavMaster)
                        }
                    }
                }
                fBRepoMasterListLiveData.postValue(listOfMasters)

            }
    }

    private fun registrationResponse(task: Task<AuthResult>) {
        if (task.isSuccessful) {
            fBRepoRegisterFbLiveData.postValue(FbAuthResult(true, ""))
        } else {
            FirebaseManager.fbAuth.signInAnonymously()
            var errorMessage = application.getString(R.string.registr__failed)
            if (task.exception != null) {
                errorMessage = try {
                    throw task.exception!!
                } catch (e: FirebaseAuthUserCollisionException) {
                    application.getString(R.string.registr__email_used)
                } catch (e: FirebaseNetworkException) {
                    application.getString(R.string.general__no_internet)
                } catch (e: Exception) {
                    "Exception: $e"
                }
            }
            fBRepoRegisterFbLiveData.postValue(FbAuthResult(false, errorMessage))
        }
    }

//    fun removeUserAccount() {
//        val user = FirebaseManager.fbAuth.currentUser!!
//        user.delete().addOnCompleteListener { task ->
//            if (task.isSuccessful) {
//                fBRepoDeleteFbUserLiveData.postValue(FbAuthResult(true, ""))
//            } else {
//                var errorMessage = ""
//                if (task.exception != null) {
//                    errorMessage = try {
//                        throw task.exception!!
//                    } catch (e: FirebaseAuthRecentLoginRequiredException) {
//                        application.getString(R.string.remove_account__require_log_in)
//                    } catch (e: Exception) {
//                        "Exception: $e"
//                    }
//                }
//                fBRepoDeleteFbUserLiveData.postValue(FbAuthResult(false, errorMessage))
//            }
//        }
//    }

    fun updateMastersLikesInFB(docId: String, likesNumber: Long) {
        firestoreDB
            .collection(AppConstants.FIRESTORE_MASTERS)
            .document(docId)
            .update(AppConstants.FRS_MASTER_LIKES_COUNTER, likesNumber)

    }

//    fun authWithCredential(credential: AuthCredential) {
//        FirebaseManager.fbAuth.currentUser?.linkWithCredential(credential)
//            ?.addOnCompleteListener { task ->
//                registrationResponse(task)
//            }
//    }
//
//    fun loginWithCredential(credential: AuthCredential) {
//        val currentUser = FirebaseManager.fbAuth.currentUser
//        currentUser?.delete()?.addOnCompleteListener {
//            FirebaseManager.fbAuth.signInWithCredential(credential).addOnCompleteListener { task ->
//                registrationResponse(task)
//            }
//        }
//
//    }

    fun getAllQuotes() {
        val listOfQuotes = ArrayList<Quote>()
        firestoreDB
            .collection(AppConstants.FIRESTORE_QUOTES)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    val quote = Quote(
                        document.get(AppConstants.FRS_QUOTES_SOURCE)
                            .toString(),
                        document.get(AppUtils.getItemWithLanguageSuffix(AppConstants.FRS_QUOTES_QUOTE))
                            .toString(),
                        document.get(AppConstants.FRS_QUOTES_AUTHOR).toString()
                    )
                    listOfQuotes.add(quote)
                }
                val sortedList = listOfQuotes.sortedBy { AppUtils.getLastXLettersFromText(8, it.quote) }
                val toArrayList = ArrayList<Quote>()
                toArrayList.addAll(sortedList)
                fBRepoQuoteListLiveData.postValue(toArrayList)
            }
    }

    fun getAllMovies() {
        val listOfMovies = ArrayList<Movie>()
        firestoreDB
            .collection(AppConstants.FIRESTORE_MOVIES)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    val movie = Movie(
                        document.get(AppConstants.FRS_MOVIES_TITLE).toString(),
                        document.get(AppConstants.FRS_MOVIES_YEAR).toString(),
                        document.get(AppConstants.FRS_MOVIES_POSTER_URL).toString(),
                        document.get(AppConstants.FRS_MOVIES_DIRECTOR).toString(),
                        stringToInt(document.get(AppConstants.FRS_MOVIES_LIKES_NO).toString())
                    )
                    listOfMovies.add(movie)
                }
                updateMoviesLocalIfNeeded(listOfMovies)
                fBRepoMovieListLiveData.postValue(listOfMovies)
            }
    }

    private fun stringToInt(stringNumber: String): Int {
        return if (stringNumber.isNotEmpty() && stringNumber != "null") stringNumber.toInt() else 0
    }

    private fun updateMoviesLocalIfNeeded(fbMovieList: ArrayList<Movie>) {
        val movieDataBase: MovieDataBase = MovieDataBase.getInstance(application)!!
        val movieLocalDao = movieDataBase.movieLocalDao()
        CoroutineScope(Dispatchers.Default).launch {
            fbMovieList.forEach { fbMovie ->
                val localMovie = movieLocalDao.getLocalMovieByTitle(fbMovie.title)
                if (localMovie.isEmpty()) {
                    movieLocalDao.insertMovieLocal(AppUtils.convertMovieToMovieLocal(fbMovie))
                } else {
                    if (localMovie[0].favLikesNo < fbMovie.favLikesNo) {
                        movieLocalDao.updateMovieLocal(
                            MovieLocal(
                                localMovie[0].id,
                                localMovie[0].title,
                                localMovie[0].year,
                                localMovie[0].posterUrl,
                                localMovie[0].director,
                                localMovie[0].toWatch,
                                localMovie[0].favorite,
                                fbMovie.favLikesNo
                            )
                        )
                    }
                }
            }
        }
    }

    fun updateMovieLikes(title: String, likesNumber: Int) {
        val collRef = firestoreDB.collection(AppConstants.FIRESTORE_MOVIES)
        collRef
            .whereEqualTo(AppConstants.FRS_MOVIES_TITLE, title)
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    for (document in task.result) {
                        collRef.document(document.id)
                            .update(AppConstants.FRS_MOVIES_LIKES_NO, likesNumber.toString())
                    }
                }
            }
    }
}