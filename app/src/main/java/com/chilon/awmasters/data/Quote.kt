package com.chilon.awmasters.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "quote_table")
data class Quote(
    val source:String,
    val quote: String,
    val author: String
) {
    @PrimaryKey(autoGenerate = true)
    var quoteId:Int = 0
}