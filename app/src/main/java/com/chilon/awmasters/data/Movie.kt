package com.chilon.awmasters.data

import androidx.room.Entity
import androidx.room.PrimaryKey


data class Movie(
    val title: String,
    val year: String,
    val posterUrl: String,
    val director: String,
    var favLikesNo:Int
) {


}