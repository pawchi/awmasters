package com.chilon.awmasters.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Quote::class], version = 2, exportSchema = false)
abstract class QuoteDataBase : RoomDatabase() {
    abstract fun quoteDao(): QuoteDao

    companion object {
        private var instance: QuoteDataBase? = null

        fun getInstance(context: Context): QuoteDataBase? {
            if (instance == null) {
                instance = Room.databaseBuilder(
                    context.applicationContext,
                    QuoteDataBase::class.java,
                    "quote_table")
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return instance
        }
    }
}