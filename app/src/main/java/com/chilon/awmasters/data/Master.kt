package com.chilon.awmasters.data

import androidx.room.Entity
import androidx.room.PrimaryKey


class Master(
    var docId:String,
    var nickName: String,
    var name: String,
    var surname: String,
    var image: String,
    var dateOfBirth: String?,
    var dateOfDeath:String?,
    var cityOfBirth: String,
    var countryOfBirth: String,
    var countryOfResidence: String,
    var primaryLanguage: String,
    var biography: String,
    var homePage:String,
    var youtube:String,
    var facebook:String,
    var events:String,
    var favorite:Boolean,
    var favLikesNo:Long?
)
{
}