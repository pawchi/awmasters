package com.chilon.awmasters.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [MovieLocal::class], version = 7, exportSchema = false)
abstract class MovieDataBase : RoomDatabase() {
    abstract fun movieLocalDao(): MovieLocalDao

    companion object {
        private var instance: MovieDataBase? = null

        fun getInstance(context: Context): MovieDataBase? {
            if (instance == null) {
                instance = Room.databaseBuilder(
                    context.applicationContext,
                    MovieDataBase::class.java,
                    "movie_table")
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return instance
        }
    }
}