package com.chilon.awmasters.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "movie_table")
data class MovieLocal(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var title: String,
    var year: String,
    var posterUrl: String,
    var director: String,
    var toWatch: Boolean?,
    var favorite: Boolean,
    var favLikesNo: Int
)