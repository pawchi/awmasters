package com.chilon.awmasters.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [MasterFav::class],version = 2,exportSchema = false)
abstract class MasterFavDataBase:RoomDatabase() {
    abstract fun masterFavDao():MasterFavDao

    companion object{
        private var instance: MasterFavDataBase? = null

        fun getInstance(context: Context):MasterFavDataBase?{
            if (instance==null){
                instance = Room.databaseBuilder(
                    context.applicationContext,
                    MasterFavDataBase::class.java,
                    "masters_fav_table")
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return instance
        }
    }
}