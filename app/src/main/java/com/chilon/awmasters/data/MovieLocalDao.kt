package com.chilon.awmasters.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface MovieLocalDao {

    @Insert
    fun insertMovieLocal(movieLocal: MovieLocal)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateMovieLocal(movieLocal: MovieLocal)

    @Delete
    fun deleteMovieLocal(movieLocal: MovieLocal)

    @Query("SELECT * FROM movie_table")
    fun getAllMoviesLocal():LiveData<List<MovieLocal>>

    @Query("SELECT * FROM movie_table WHERE title=:title")
    fun getLocalMoviesList(title:String):LiveData<List<MovieLocal>>

    @Query("SELECT * FROM movie_table WHERE title=:title")
    fun getLocalMovieByTitle(title: String):List<MovieLocal>

    @Query("SELECT * FROM movie_table")
    fun getAllMoviesLocalOneTime():List<MovieLocal>

}