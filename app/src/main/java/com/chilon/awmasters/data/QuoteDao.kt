package com.chilon.awmasters.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface QuoteDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertQuote(quote: Quote)

    @Query("DELETE FROM quote_table")
    fun deleteAllQuotes()

    @Query("SELECT * FROM quote_table")
    fun getAllQuotes():LiveData<List<Quote>>
}