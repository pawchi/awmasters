package com.chilon.awmasters.data

import android.provider.ContactsContract
import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface MasterFavDao {

    @Insert
    fun insertMasterFav(masterFav: MasterFav)

    @Update
    fun updateMasterFav(masterFav: MasterFav)

    @Delete
    fun deleteMasterFav(masterFav: MasterFav)

    @Query("DELETE FROM masters_fav_table")
    fun deleteAllMasterFav()

    @Query("SELECT * FROM masters_fav_table")
    fun getAllMastersFav():LiveData<List<MasterFav>>

    @Query("SELECT * FROM masters_fav_table WHERE nickName =:nickname")
    fun getFav(nickname: String):List<MasterFav>

    @Query("SELECT * FROM masters_fav_table WHERE nickName =:nickname")
    fun getMasterFavWithNickName(nickname: String):LiveData<List<MasterFav>>

    @Query("DELETE FROM masters_fav_table WHERE nickName =:nickname")
    fun deleteMasterFavWithNickName(nickname: String)

}