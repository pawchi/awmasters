package com.chilon.awmasters.view

import android.content.ContentValues.TAG
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.chilon.awmasters.R
import com.chilon.awmasters.data.FirebaseManager
import com.chilon.awmasters.data.Master
import com.chilon.awmasters.utilis.AppConstants
import com.chilon.awmasters.utilis.AppUtils
import kotlin.collections.ArrayList

class ShowMastersAdapter(
    private val masterList: ArrayList<Master>,
    private val itemListener: OnItemClickListener,
    private val highlightText: String
) : RecyclerView.Adapter<ShowMastersAdapter.ShowMasterViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowMasterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_master, parent, false)
        return ShowMasterViewHolder(view)
    }

    @ExperimentalStdlibApi
    override fun onBindViewHolder(holder: ShowMasterViewHolder, position: Int) {
        holder.bindData(masterList[position], position)
    }

    override fun getItemCount(): Int {
        return masterList.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    inner class ShowMasterViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        @ExperimentalStdlibApi
        fun bindData(master: Master, position: Int) {
            val masterBornCountry = itemView.findViewById<TextView>(R.id.item_master_born_at_country)
            val masterBornCountryLabel = itemView.findViewById<TextView>(R.id.item_master_born_country_label)
            val masterLivesIn = itemView.findViewById<TextView>(R.id.item_master_lives_in_country)
            val masterLivesInOrDiedInLabel = itemView.findViewById<TextView>(R.id.item_master_lives_in_label)
            val masterAge = itemView.findViewById<TextView>(R.id.item_master_age)
            val masterAgeLabel = itemView.findViewById<TextView>(R.id.item_master_age_label)
            val masterBiographyMoreTextView = itemView.findViewById<TextView>(R.id.item_master_biography_more_text_view)
            //val isAlive = itemView.findViewById<ImageView>(R.id.item_master_image_dead_cross)
            val addToFavoriteLayout = itemView.findViewById<ConstraintLayout>(R.id.item_master_lin_layout_fav)
            val masterImageFav = itemView.findViewById<ImageView>(R.id.item_master_favorites)
            val masterLikeCounter = itemView.findViewById<TextView>(R.id.item_master_like_count_textview)
            masterImageFav.isSelected = master.favorite

            val masterImage = itemView.findViewById<com.google.android.material.imageview.ShapeableImageView>(R.id.item_master_image)
            val masterName = itemView.findViewById<TextView>(R.id.item_master_name)
            val masterNickName = itemView.findViewById<TextView>(R.id.item_master_nick_name)
            val masterNo = itemView.findViewById<TextView>(R.id.item_master_no)
            val masterSurName = itemView.findViewById<TextView>(R.id.item_master_surname)
            val masterSpokenLang = itemView.findViewById<TextView>(R.id.item_master_spoken_languages)
            val masterSpokenLangLabel = itemView.findViewById<TextView>(R.id.item_master_spoken_languages_label)
            val masterFacebook = itemView.findViewById<TextView>(R.id.item_master_facebook)
            val masterVideo = itemView.findViewById<TextView>(R.id.item_master_video)
            val masterEvents = itemView.findViewById<TextView>(R.id.item_master_events)
            val masterWww = itemView.findViewById<TextView>(R.id.item_master_www)

            Glide.with(itemView.context)
                .load(master.image)
                .transition(DrawableTransitionOptions.withCrossFade())
                .centerCrop()
                .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .apply(RequestOptions.overrideOf(150, 200))
                .error(R.drawable.ic_person_not_found)
                .dontAnimate()
                .into(masterImage)

            masterNickName.text = AppUtils.capitalizeFirstLetterOfEachWordAndAfterDash(master.nickName)

            val displayedPosition = position+1
            masterNo.text = displayedPosition.toString()


            if (master.dateOfDeath == itemView.context.getString(R.string.general__unknown)) {
                //isAlive.visibility = View.GONE
                masterAge.text = AppUtils.getAge(masterList[position].dateOfBirth, itemView.context)
                masterLivesInOrDiedInLabel.text =
                    itemView.context.getString(R.string.general__lives_in)
            } else {
                //isAlive.visibility = View.VISIBLE
                masterAge.text = itemView.context.getString(R.string.general__dead)
                masterLivesInOrDiedInLabel.text =
                    itemView.context.getString(R.string.general__died_in)
            }


            masterLikeCounter.text = master.favLikesNo.toString()

            masterName.text = AppUtils.ifNoTextGetUnknown(itemView.context,master.name)
            masterSurName.text = AppUtils.ifNoTextGetUnknown(itemView.context,master.surname)
            masterBornCountry.text = AppUtils.ifNoTextGetUnknown(itemView.context,master.countryOfBirth)
            masterLivesIn.text = AppUtils.ifNoTextGetUnknown(itemView.context,master.countryOfResidence)
            masterAge.text = AppUtils.ifNoTextGetUnknown(itemView.context,masterAge.text.toString())
            masterSpokenLang.text = AppUtils.ifNoTextGetUnknown(itemView.context,master.primaryLanguage)
            masterBiographyMoreTextView.text = AppUtils.ifNoTextGetUnknown(itemView.context,master.biography)

            //hide all unknown fields and labels
            if (masterBornCountry.text==itemView.context.getString(R.string.general__unknown)){
                masterBornCountry.visibility = View.GONE
                masterBornCountryLabel.visibility = View.GONE
            }
            if (masterLivesIn.text==itemView.context.getString(R.string.general__unknown)){
                masterLivesIn.visibility = View.GONE
                masterLivesInOrDiedInLabel.visibility = View.GONE
            }
            if (masterAge.text==itemView.context.getString(R.string.general__unknown)){
                masterAge.visibility=View.GONE
                masterAgeLabel.visibility=View.GONE
            }
            if (masterSpokenLang.text==itemView.context.getString(R.string.general__unknown)){
                masterSpokenLang.visibility=View.GONE
                masterSpokenLangLabel.visibility=View.GONE
            }

            if (master.facebook=="" || master.facebook=="null"){
                masterFacebook.visibility = View.GONE
            }
            if (master.youtube==""||master.youtube=="null"){
                masterVideo.visibility=View.GONE
            }
            if (master.events==""||master.events=="null"){
                masterEvents.visibility=View.GONE
            }

            if (highlightText != "") {
                AppUtils.setHighLightedText(masterNickName, highlightText)
                AppUtils.setHighLightedText(masterSpokenLang, highlightText)
                AppUtils.setHighLightedText(masterLivesIn, highlightText)
                AppUtils.setHighLightedText(masterBornCountry, highlightText)
                AppUtils.setHighLightedText(masterSurName, highlightText)
                AppUtils.setHighLightedText(masterName, highlightText)
                AppUtils.setHighLightedText(masterBiographyMoreTextView, highlightText)
            }

            if (masterList.size<getSharedPrefsInt(itemView)){
                AppUtils.expandTextView(masterBiographyMoreTextView)
                }

            masterNickName.setOnClickListener { expandBiography(masterBiographyMoreTextView)}
            masterImage.setOnClickListener { expandBiography(masterBiographyMoreTextView) }
            masterBiographyMoreTextView.setOnClickListener { expandBiography(masterBiographyMoreTextView) }

            masterFacebook.setOnClickListener {
                itemListener.onFacebookClick(master.facebook)
            }
            masterVideo.setOnClickListener {
                itemListener.onYoutubeClick(master.youtube)
            }
            masterWww.setOnClickListener {
                itemListener.onWwwClick(master.homePage)
            }
            masterEvents.setOnClickListener {
                itemListener.onEventsClick(master.events)
            }

            addToFavoriteLayout.setOnClickListener {
                val tempMaster = masterList[position]
                if (masterImageFav.isSelected) {
                    tempMaster.favLikesNo = masterList[position].favLikesNo?.minus(1)
                    masterList[position] = tempMaster
                } else {
                    tempMaster.favLikesNo = masterList[position].favLikesNo?.plus(1)
                    masterList[position] = tempMaster
                }
                itemListener.onAddToFavoriteClick(position, masterList)
            }

            val currentUser = FirebaseManager.fbAuth.currentUser
            if (currentUser != null) {
                masterLikeCounter.visibility = View.VISIBLE
            } else {
                masterLikeCounter.visibility = View.GONE
            }

        }
    }

    interface OnItemClickListener {
        fun onFacebookClick(facebookLink: String)
        fun onYoutubeClick(youtubeLink: String)
        fun onWwwClick(url: String)
        fun onAddToFavoriteClick(position: Int, masterList: ArrayList<Master>)
        fun onEventsClick(events: String)
    }

    private fun getSharedPrefsInt(view:View):Int{
        val sharedPref = view.context.getSharedPreferences(
            AppConstants.SHARED_PREFS_POOL,
            Context.MODE_PRIVATE
        )
        return sharedPref?.getInt(AppConstants.MASTER_LIST_SIZE, 1) ?: 1
    }

    private fun expandBiography(biography:TextView){
        if (biography.maxLines<5){
            AppUtils.expandTextView(biography)
        }
        if (biography.maxLines>4){
            AppUtils.collapseTextView(biography)
        }
    }

}