package com.chilon.awmasters.view

import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.chilon.awmasters.R
import com.chilon.awmasters.data.MovieLocal
import com.chilon.awmasters.utilis.AppUtils

class MoviesAdapter(
    private val moviesList: ArrayList<MovieLocal>,
    private val onItemClickListener: OnItemClickListener,
    private val highlightText: String
) : RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
        return MoviesViewHolder(view)
    }

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        holder.bindData(moviesList[position], position)
    }

    override fun getItemCount(): Int {
        return moviesList.size
    }

    inner class MoviesViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        @OptIn(ExperimentalStdlibApi::class)
        fun bindData(movie: MovieLocal, position: Int) {

            itemView.findViewById<TextView>(R.id.item_movie_title).text = movie.title
            itemView.findViewById<TextView>(R.id.item_movie_year).text = movie.year
            itemView.findViewById<TextView>(R.id.item_movie_director).text = if (movie.director!="null") movie.director else "unknown"
            itemView.findViewById<TextView>(R.id.item_movie_like_count_textview).text = movie.favLikesNo.toString()
            itemView.findViewById<ImageView>(R.id.item_movie_image_favorites).isSelected = movie.favorite

            itemView.findViewById<RadioButton>(R.id.item_movie_rb1).isChecked = if (movie.toWatch == null || movie.toWatch == false)  false else true

            if (itemView.findViewById<TextView>(R.id.item_movie_title).text.count() > 25) itemView.findViewById<TextView>(R.id.item_movie_title).setTextSize(
                TypedValue.COMPLEX_UNIT_SP, 18F
            )

            Glide.with(itemView.context)
                .load(movie.posterUrl)
                .transition(DrawableTransitionOptions.withCrossFade())
                .centerCrop()
                .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .apply(RequestOptions.overrideOf(168, 230))
                .error(R.drawable.ic_movie_not_found)
                .into(itemView.findViewById<com.google.android.material.imageview.ShapeableImageView>(R.id.item_movie_image))

            if (highlightText != "") {
                AppUtils.setHighLightedText(itemView.findViewById<TextView>(R.id.item_movie_title), highlightText)
                AppUtils.setHighLightedText(itemView.findViewById<TextView>(R.id.item_movie_director), highlightText)
                AppUtils.setHighLightedText(itemView.findViewById<TextView>(R.id.item_movie_year), highlightText)
            }

            itemView.findViewById<RadioButton>(R.id.item_movie_rb1).setOnClickListener {
                onItemClickListener.movieToWatch(moviesList[position])
            }

            itemView.findViewById<androidx.constraintlayout.widget.ConstraintLayout>(R.id.item_movie_constr_layout_fav).setOnClickListener {
                onItemClickListener.likesNumber(position, moviesList[position])
            }
        }

    }

    interface OnItemClickListener {
        fun likesNumber(position: Int, movie: MovieLocal)
        fun movieToWatch(movie: MovieLocal)
    }
}