package com.chilon.awmasters.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.chilon.awmasters.R
import com.chilon.awmasters.data.FirebaseManager
import com.chilon.awmasters.data.MasterFav
import com.chilon.awmasters.utilis.AppUtils

class ShowMastersFavAdapter(
    private val masterFavList: List<MasterFav>,
    private val itemClickListener: OnItemClickListener
) : RecyclerView.Adapter<ShowMastersFavAdapter.ShowMasterFavViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowMasterFavViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_master,parent,false)
        return ShowMasterFavViewHolder(view)
    }

    override fun onBindViewHolder(holder: ShowMasterFavViewHolder, position: Int) {
        holder.bindData(position)
    }

    override fun getItemCount(): Int {
        return masterFavList.size
    }

    inner class ShowMasterFavViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindData(position: Int){
            val masterFavImage = itemView.findViewById<ImageView>(R.id.item_master_image)
            val masterFavNickName = itemView.findViewById<TextView>(R.id.item_master_nick_name)
            val masterFavName = itemView.findViewById<TextView>(R.id.item_master_name)
            val masterFavSurname = itemView.findViewById<TextView>(R.id.item_master_surname)
            val masterFavLanguages = itemView.findViewById<TextView>(R.id.item_master_spoken_languages)
            val masterFavBornCountry = itemView.findViewById<TextView>(R.id.item_master_born_at_country)
            val masterFavLivesIn = itemView.findViewById<TextView>(R.id.item_master_lives_in_country)
            val masterFavDiedInLabel = itemView.findViewById<TextView>(R.id.item_master_lives_in_label)
            val masterFavAge = itemView.findViewById<TextView>(R.id.item_master_age)
            val masterFavBiography = itemView.findViewById<TextView>(R.id.item_master_biography_more_text_view)
            val masterFavRemoveFavorite = itemView.findViewById<ConstraintLayout>(R.id.item_master_lin_layout_fav)
            val masterFavRedHeart = itemView.findViewById<ImageView>(R.id.item_master_favorites)
            val masterFavLikeCounter = itemView.findViewById<TextView>(R.id.item_master_like_count_textview)
            val masterFavNo = itemView.findViewById<TextView>(R.id.item_master_no)
            val masterFavFacebook = itemView.findViewById<TextView>(R.id.item_master_facebook)
            val masterFavVideo = itemView.findViewById<TextView>(R.id.item_master_video)
            val masterFavEvents = itemView.findViewById<TextView>(R.id.item_master_events)
            val masterFavWww = itemView.findViewById<TextView>(R.id.item_master_www)

            Glide.with(itemView.context)
                .load(masterFavList[position].image)
                .transition(DrawableTransitionOptions.withCrossFade())
                .centerCrop()
                .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .apply(RequestOptions.overrideOf(150, 200))
                .error(R.drawable.ic_error)
                .into(masterFavImage)

            val displayedPosition = position+1
            masterFavNo.text = displayedPosition.toString()

            masterFavRedHeart.isSelected = true
            masterFavNickName.text = AppUtils.capitalizeFirstLetterOfEachWordAndAfterDash(masterFavList[position].nickName)

            if(masterFavList[position].name == "null" || masterFavList[position].name == "") masterFavName.text =
                itemView.context.getString(R.string.general__unknown) else
                masterFavName.text = masterFavList[position].name

            if(masterFavList[position].surname == "null" || masterFavList[position].surname == "") masterFavSurname.text =
                itemView.context.getString(R.string.general__unknown) else
                masterFavSurname.text = masterFavList[position].surname

            masterFavLanguages.text = masterFavList[position].primaryLanguage
            masterFavBornCountry.text = masterFavList[position].countryOfBirth
            masterFavLivesIn.text = masterFavList[position].countryOfResidence
            masterFavBiography.text = masterFavList[position].biography
            masterFavLikeCounter.text = masterFavList[position].favLikesNo.toString()

            if (masterFavList[position].dateOfDeath==itemView.context.getString(R.string.general__unknown)){
                masterFavAge.text = AppUtils.getAge(masterFavList[position].dateOfBirth,itemView.context)
            } else {
                masterFavAge.text = itemView.context.getString(R.string.general__dead)
                masterFavDiedInLabel.text = itemView.context.getString(R.string.general__died_in)
            }

            masterFavBiography.setOnClickListener {
                if (masterFavBiography.maxLines<5){
                    AppUtils.expandTextView(masterFavBiography)
                }
                if (masterFavBiography.maxLines>4){
                    AppUtils.collapseTextView(masterFavBiography)
                }
            }

            masterFavRemoveFavorite.setOnClickListener {
                itemClickListener.onRemoveFavClick(position)
            }

            masterFavFacebook.setOnClickListener {
                itemClickListener.onFacebookClick(masterFavList[position].facebook)
            }

            masterFavVideo.setOnClickListener {
                itemClickListener.onYoutubeClick(masterFavList[position].youtube)
            }
            masterFavWww.setOnClickListener {
                itemClickListener.onWwwClick(masterFavList[position].homePage)
            }
            masterFavEvents.setOnClickListener {
                itemClickListener.onEventsClick(masterFavList[position].events)
            }

            val currentUser = FirebaseManager.fbAuth.currentUser
            if (currentUser != null) {
                masterFavLikeCounter.visibility = View.VISIBLE
            } else {
                masterFavLikeCounter.visibility = View.GONE
            }
        }
    }

    interface OnItemClickListener{
        fun onFacebookClick(facebookLink:String)
        fun onRemoveFavClick(position: Int)
        fun onYoutubeClick(youtubeLink: String)
        fun onWwwClick(url: String)
        fun onEventsClick(events:String)
    }
}