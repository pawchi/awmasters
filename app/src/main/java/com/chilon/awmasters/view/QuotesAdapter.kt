package com.chilon.awmasters.view

import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipboardManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.chilon.awmasters.MainActivity
import com.chilon.awmasters.R
import com.chilon.awmasters.data.Quote

class QuotesAdapter(private val quotesList: ArrayList<Quote>) :
    RecyclerView.Adapter<QuotesAdapter.QuotesViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuotesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_quote, parent, false)
        return QuotesViewHolder(view)
    }

    override fun onBindViewHolder(holder: QuotesViewHolder, position: Int) {
        holder.bindData(position)
    }

    override fun getItemCount(): Int {
        return quotesList.size
    }

    inner class QuotesViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        @SuppressLint("UseCompatLoadingForDrawables")
        fun bindData(position: Int) {
            val source = itemView.findViewById<TextView>(R.id.item_quote__source)
            val quote = itemView.findViewById<TextView>(R.id.item_quote__quote)
            val author = itemView.findViewById<TextView>(R.id.item_quote__author)
            val shareQuote = itemView.findViewById<ImageView>(R.id.item_quote__share)
            val copyToClipboard = itemView.findViewById<ImageView>(R.id.item_quote__copy)
            val relativeLayout = itemView.findViewById<RelativeLayout>(R.id.quote_relative_layout)

            if (position==0){
                relativeLayout.background = itemView.context.getDrawable(R.drawable.shape_brown_border_quote)
            } else{
                relativeLayout.background = itemView.context.getDrawable(R.drawable.background_item_master)
            }

            if (quotesList[position].source == "") {
                source.text = ""
            } else {
                source.text = quotesList[position].source
            }

            quote.text = quotesList[position].quote

            if (quotesList[position].author == "") {
                author.text = itemView.context.getString(R.string.fr_quotes__author_unknown)
            } else {
                author.text = quotesList[position].author
            }
            if (quotesList[position].source==""){
                source.text = itemView.context.getString(R.string.general__unknown)
            }

            shareQuote.setOnClickListener {
                (itemView.context as MainActivity).shareWithFriends(quote.text.toString())
            }

            copyToClipboard.setOnClickListener {
                val clipboard = ContextCompat.getSystemService(itemView.context, ClipboardManager::class.java)
                clipboard?.setPrimaryClip(ClipData.newPlainText("",quote.text.toString()))
                Toast.makeText(itemView.context, itemView.context.getString(R.string.fr_quotes__copy_text),Toast.LENGTH_LONG).show()
            }

        }
    }
}