package com.chilon.awmasters.view

import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.chilon.awmasters.R
import com.chilon.awmasters.data.MovieLocal

class MoviesFavAdapter(
    private val list: MutableList<MovieLocal>,
    private val onItemClickListener: OnItemClickListener,
    private val highlightText: String
) :
    RecyclerView.Adapter<MoviesFavAdapter.MoviesFavViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesFavViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
        return MoviesFavViewHolder(view)
    }

    override fun onBindViewHolder(holder: MoviesFavViewHolder, position: Int) {
        holder.bindData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MoviesFavViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindData(movie: MovieLocal) {
            val itemMovieTitle = itemView.findViewById<TextView>(R.id.item_movie_title)
            val movieYear = itemView.findViewById<TextView>(R.id.item_movie_year)
            val movieLikeCount = itemView.findViewById<TextView>(R.id.item_movie_like_count_textview)
            val movieImageFav = itemView.findViewById<ImageView>(R.id.item_movie_image_favorites)
            val movieDirector = itemView.findViewById<TextView>(R.id.item_movie_director)
            val movieRadioButton = itemView.findViewById<RadioButton>(R.id.item_movie_rb1)
            val movieImage = itemView.findViewById<com.google.android.material.imageview.ShapeableImageView>(R.id.item_movie_image)

            if (movie.toWatch != null) {
                if (movie.toWatch == false) {
                    itemView.visibility = View.GONE
                } else {
                    itemMovieTitle.text = movie.title
                    movieYear.text = movie.year
                    movieLikeCount.text = movie.favLikesNo.toString()
                    movieImageFav.isSelected = movie.favorite
                    movieDirector.text = if (movie.director!="null") movie.director else "unknown"
                    if (movie.toWatch == true) {
                        itemView.setBackgroundColor(
                            ContextCompat.getColor(itemView.context, R.color.color_light_orange))
                        movieRadioButton.isChecked = true
                    } else {
                        itemView.setBackgroundColor(ContextCompat.getColor(
                                itemView.context,
                                R.color.color_background_light_violet))
                        movieRadioButton.isChecked = false
                    }

                    if (itemMovieTitle.text.count() > 25) itemMovieTitle.setTextSize(
                        TypedValue.COMPLEX_UNIT_SP, 18F
                    )

                    Glide.with(itemView.context)
                        .load(movie.posterUrl)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .centerCrop()
                        .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                        .apply(RequestOptions.overrideOf(168, 230))
                        .error(R.drawable.ic_error)
                        .into(movieImage)

                    movieRadioButton.setOnClickListener {
                        onItemClickListener.movieToWatch(movie)
                    }
                }
            }
        }

    }

    interface OnItemClickListener {
        fun movieToWatch(movie:MovieLocal)
    }
}